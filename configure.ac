#
#  This file is part of gop-cotesdex.
#
#  Copyright (C) 2015 Karl Lindén <lilrc@users.sourceforge.net>
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 3 of the License, or
#  (at your option) any later version.
#  
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#  
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#  

AC_PREREQ([2.69])
AC_INIT(
    [gop-cotesdex],
    [3.1.1],
    [https://bitbucket.org/lilrc/gop-cotesdex/issues],
    [gop-cotesdex],
    [https://bitbucket.org/lilrc/gop-cotesdex])
AC_CONFIG_SRCDIR([examples/simple-parse.c])
AC_CONFIG_HEADERS([config.h])
AC_CONFIG_AUX_DIR([build-aux])

AM_INIT_AUTOMAKE([subdir-objects])

# Checks for programs.
AC_PROG_CC
AC_PROG_CC_C99
AM_PROG_CC_C_O
PKG_PROG_PKG_CONFIG

COTESDEX_INIT
COTESDEX_PROG_COTESDEX

# Let the user decide whether or not to build the documentation. Default is not
# to build it.
AC_ARG_ENABLE(
    [doc],
    [AS_HELP_STRING(
        [--enable-doc],
        [build documentation (default is no)]
    )],
)

AS_IF([test x$enable_doc = xyes], [COTESDEX_PROG_ASCIIDOC])
AM_CONDITIONAL([ENABLE_DOC], [test x$enable_doc = xyes])

AC_ARG_ENABLE(
    [examples],
    [AS_HELP_STRING(
        [--enable-examples],
        [install source code examples (default is yes)]
    )],
)
AM_CONDITIONAL([ENABLE_EXAMPLES], [test x$enable_examples != xno])

# Checks for libraries.
PKG_CHECK_MODULES(
    [gop],
    [gop-3],
    [],
    [AC_MSG_ERROR(
[gop was not found.
Please download and install it from <https://bitbucket.org/lilrc/gop>])])

# Checks for header files.

# Checks for typedefs, structures, and compiler characteristics.

# Checks for library functions.

AC_CONFIG_FILES([Makefile])
AC_OUTPUT
