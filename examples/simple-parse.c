/* @cotesdex license
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * @cotesdex */

/* @cotesdex title A simple GOP program */
/* @cotesdex description
 * This example shows the very basic functionality of GOP. It does the
 * following:
 * 
 * . Create a GOP context using +gop_new()+.
 * . Feed the GOP context with and option table using +gop_add_table()+.
 * . Parse the options using +gop_parse_options()+.
 * . Print the result.
 * . Destroy the context using +gop_destroy()+.
 * @cotesdex */

/* @cotesdex hide
 * First tests that GOP should handle...
 * @cotesdex unhide */

/* @cotesdex test
 *  @cotesdex status 0
 *  @cotesdex stderr
 *  @cotesdex stdout */

/* @cotesdex test
 *  @cotesdex cmdline
 *  --string=Hello --integer=75
 *  @cotesdex
 *  @cotesdex status 0
 *  @cotesdex stderr
 *  @cotesdex stdout
 *   @cotesdex content
 *   integer = 75
 *   string = Hello
 *   @cotesdex */

/* @cotesdex test
 *  @cotesdex cmdline
 *  -s "Many words in quote"
 *  @cotesdex
 *  @cotesdex status 0
 *  @cotesdex stderr
 *  @cotesdex stdout
 *   @cotesdex content
 *   string = Many words in quote
 *   @cotesdex
 */

/* @cotesdex test
 *  @cotesdex cmdline
 *  -i 39
 *  @cotesdex
 *  @cotesdex status 0
 *  @cotesdex stderr
 *  @cotesdex stdout
 *   @cotesdex content
 *   integer = 39
 *   @cotesdex */

/* @cotesdex test
 *  @cotesdex cmdline
 *  -i 1793 -s "One can never test too much"
 *  @cotesdex
 *  @cotesdex status 0
 *  @cotesdex stderr
 *  @cotesdex stdout
 *   @cotesdex content
 *   integer = 1793
 *   string = One can never test too much
 *   @cotesdex */

/* @cotesdex test
 *  @cotesdex cmdline
 *  --string "I like kittens" --integer 99997
 *  @cotesdex
 *  @cotesdex status 0
 *  @cotesdex stderr
 *  @cotesdex stdout
 *   @cotesdex content
 *   integer = 99997
 *   string = I like kittens
 *   @cotesdex */

/* @cotesdex hide
 * ... Then some situations GOP should not handle. In all these cases stdout
 * should be left clean.
 * @cotesdex unhide */

/* @cotesdex test
 *  @cotesdex status 1
 *  @cotesdex cmdline
 *  --string
 *  @cotesdex
 *  @cotesdex stdout */

/* @cotesdex test
 *  @cotesdex status 1
 *  @cotesdex cmdline
 *  --integer
 *  @cotesdex
 *  @cotesdex stdout */

/* @cotesdex test
 *  @cotesdex status 1
 *  @cotesdex cmdline
 *  -is
 *  @cotesdex
 *  @cotesdex stdout */

/* @cotesdex test
 *  @cotesdex cmdline
 *  -is 1793 "This is not allowed"
 *  @cotesdex
 *  @cotesdex status 1
 *  @cotesdex stdout */

/* Standard headers. */
#include <stdio.h>
#include <stdlib.h>

/* gop.h must be included to use GOP. */
#include <gop.h>

int
main(int argc, char ** const argv)
{
    /* This is the GOP context that will be used. */
    gop_t * gop;

    /* An integer that GOP will fill in. */
    int integer = -1;

    /* This is a string GOP will filled in. The string must not be free'd as it
     * is directly taken from argv. */
    char * string = NULL;

    /* This is option table that is going to be used. */
    const gop_option_t options[] = {
        {"integer", 'i', GOP_INT, &integer, NULL,
            "Print an integer", "<integer>"},
        {"string", 's', GOP_STRING, &string, NULL,
            "Print a string", "<string>"},
        GOP_TABLEEND
    };

    /* Get a new GOP context. */
    gop = gop_new();
    if (gop == NULL) {
        /* gop_new() returns NULL on failure. */
        return EXIT_FAILURE;
    }

    /* Add the option table. */
    gop_add_table(gop, NULL, options);

    /* Parse the options. */
    gop_parse(gop, &argc, argv);

    /* Destroy the GOP context, because it is not needed anymore. */
    gop_destroy(gop);

    /* When GOP is done it should have written the string given with --string to
     * the variable string. In addition it should have filled in the integer
     * passed to the program with --integer to the integer variable. If the user
     * has not supplied any of these the default values (-1 and NULL) are
     * retained. */

    /* Print the integer if it is not -1 (the default). */
    if (integer != -1) {
        printf("integer = %d\n", integer);
    }

    /* Print the string if it is not NULL (the default). */
    if (string != NULL) {
        printf("string = %s\n", string);
    }

    return EXIT_SUCCESS;
}
