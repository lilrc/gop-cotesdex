/* @cotesdex license
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * @cotesdex */

/* @cotesdex title An exampe of yesno options */
/* @cotesdex description
 * Prerequisites:
 *  simple-parse
 *  autohelp
 * 
 * This example shows how GOP yesno options work.
 * 
 * . Create a GOP context using +gop_new()+.
 * . Feed the GOP context with an option table using +gop_add_table()+. The 
 *   table includes both yesno options.
 * . Add the automatic help output option table using +gop_autohelp()+.
 * . Parse the options using +gop_parse_options()+.
 * . Print the result.
 * . Destroy the context using +gop_destroy()+.
 * @cotesdex */

/* @cotesdex hide
 * First the tests that GOP should handle... Standard error should be clean in
 * almost all cases.
 * @cotesdex unhide */

/* @cotesdex test
 *  @cotesdex status 0
 *  @cotesdex stderr
 *  @cotesdex stdout
 *   @cotesdex content
 *   cat
 *   horse
 *   @cotesdex */

/* @cotesdex test
 *  @cotesdex cmdline
 *  --help
 *  @cotesdex
 *  @cotesdex status 0
 *  @cotesdex stdout
 *   @cotesdex content
 *   Usage: yesno [OPTION...]
 *   
 *     -c  no|yes            Print 'cat' [yes]
 *     -d, --dog=no|yes      Print 'dog' [no]
 *         --horse=no|yes    Print 'horse' [yes]
 *   
 *   Help options:
 *     -?, --help            Show this help message
 *         --usage           Display brief usage message
 *   @cotesdex */

/* @cotesdex test
 *  @cotesdex cmdline
 *  -?
 *  @cotesdex
 *  @cotesdex status 0
 *  @cotesdex stdout
 *   @cotesdex content
 *   Usage: yesno [OPTION...]
 *   
 *     -c  no|yes            Print 'cat' [yes]
 *     -d, --dog=no|yes      Print 'dog' [no]
 *         --horse=no|yes    Print 'horse' [yes]
 *   
 *   Help options:
 *     -?, --help            Show this help message
 *         --usage           Display brief usage message
 *   @cotesdex */

/* @cotesdex test
 *  @cotesdex cmdline
 *  --usage
 *  @cotesdex
 *  @cotesdex status 0
 *  @cotesdex stdout
 *   @cotesdex content
 *   Usage: yesno [-?] [-c no|yes] [-d|--dog=no|yes] [--horse=no|yes] [-?|--help]
 *       [--usage]
 *   @cotesdex */

/* @cotesdex test
 *  @cotesdex cmdline
 *  -c yes
 *  @cotesdex
 *  @cotesdex status 0
 *  @cotesdex stderr
 *  @cotesdex stdout
 *   @cotesdex content
 *   cat
 *   horse
 *   @cotesdex */

/* @cotesdex test
 *  @cotesdex cmdline
 *  -c no
 *  @cotesdex
 *  @cotesdex status 0
 *  @cotesdex stderr
 *  @cotesdex stdout
 *   @cotesdex content
 *   horse
 *   @cotesdex */

/* @cotesdex test
 *  @cotesdex cmdline
 *  -d yes
 *  @cotesdex
 *  @cotesdex status 0
 *  @cotesdex stderr
 *  @cotesdex stdout
 *   @cotesdex content
 *   cat
 *   dog
 *   horse
 *   @cotesdex */

/* @cotesdex test
 *  @cotesdex cmdline
 *  --dog yes
 *  @cotesdex
 *  @cotesdex status 0
 *  @cotesdex stderr
 *  @cotesdex stdout
 *   @cotesdex content
 *   cat
 *   dog
 *   horse
 *   @cotesdex */

/* @cotesdex test
 *  @cotesdex cmdline
 *  --dog=yes
 *  @cotesdex
 *  @cotesdex status 0
 *  @cotesdex stderr
 *  @cotesdex stdout
 *   @cotesdex content
 *   cat
 *   dog
 *   horse
 *   @cotesdex */

/* @cotesdex test
 *  @cotesdex cmdline
 *  -d no
 *  @cotesdex
 *  @cotesdex status 0
 *  @cotesdex stderr
 *  @cotesdex stdout
 *   @cotesdex content
 *   cat
 *   horse
 *   @cotesdex */

/* @cotesdex test
 *  @cotesdex cmdline
 *  --dog no
 *  @cotesdex
 *  @cotesdex status 0
 *  @cotesdex stderr
 *  @cotesdex stdout
 *   @cotesdex content
 *   cat
 *   horse
 *   @cotesdex */

/* @cotesdex test
 *  @cotesdex cmdline
 *  --dog=no
 *  @cotesdex
 *  @cotesdex status 0
 *  @cotesdex stderr
 *  @cotesdex stdout
 *   @cotesdex content
 *   cat
 *   horse
 *   @cotesdex */

/* @cotesdex test
 *  @cotesdex cmdline
 *  --horse yes
 *  @cotesdex
 *  @cotesdex status 0
 *  @cotesdex stderr
 *  @cotesdex stdout
 *   @cotesdex content
 *   cat
 *   horse
 *   @cotesdex */

/* @cotesdex test
 *  @cotesdex cmdline
 *  --horse=yes
 *  @cotesdex
 *  @cotesdex status 0
 *  @cotesdex stderr
 *  @cotesdex stdout
 *   @cotesdex content
 *   cat
 *   horse
 *   @cotesdex */

/* @cotesdex test
 *  @cotesdex cmdline
 *  --horse no
 *  @cotesdex
 *  @cotesdex status 0
 *  @cotesdex stderr
 *  @cotesdex stdout
 *   @cotesdex content
 *   cat
 *   @cotesdex */

/* @cotesdex test
 *  @cotesdex cmdline
 *  --horse=no
 *  @cotesdex
 *  @cotesdex status 0
 *  @cotesdex stderr
 *  @cotesdex stdout
 *   @cotesdex content
 *   cat
 *   @cotesdex */
 
/* @cotesdex test
 *  @cotesdex cmdline
 *  -c yes -d yes
 *  @cotesdex
 *  @cotesdex status 0
 *  @cotesdex stderr
 *  @cotesdex stdout
 *   @cotesdex content
 *   cat
 *   dog
 *   horse
 *   @cotesdex */

/* @cotesdex test
 *  @cotesdex cmdline
 *  -c yes --dog yes
 *  @cotesdex
 *  @cotesdex status 0
 *  @cotesdex stderr
 *  @cotesdex stdout
 *   @cotesdex content
 *   cat
 *   dog
 *   horse
 *   @cotesdex */

/* @cotesdex test
 *  @cotesdex cmdline
 *  -c yes --dog=yes
 *  @cotesdex
 *  @cotesdex status 0
 *  @cotesdex stderr
 *  @cotesdex stdout
 *   @cotesdex content
 *   cat
 *   dog
 *   horse
 *   @cotesdex */

/* @cotesdex test
 *  @cotesdex cmdline
 *  -c no -d yes
 *  @cotesdex
 *  @cotesdex status 0
 *  @cotesdex stderr
 *  @cotesdex stdout
 *   @cotesdex content
 *   dog
 *   horse
 *   @cotesdex */

/* @cotesdex test
 *  @cotesdex cmdline
 *  -c no --dog yes
 *  @cotesdex
 *  @cotesdex status 0
 *  @cotesdex stderr
 *  @cotesdex stdout
 *   @cotesdex content
 *   dog
 *   horse
 *   @cotesdex */

/* @cotesdex test
 *  @cotesdex cmdline
 *  -c no --dog=yes
 *  @cotesdex
 *  @cotesdex status 0
 *  @cotesdex stderr
 *  @cotesdex stdout
 *   @cotesdex content
 *   dog
 *   horse
 *   @cotesdex */

/* @cotesdex test
 *  @cotesdex cmdline
 *  -c yes -d no
 *  @cotesdex
 *  @cotesdex status 0
 *  @cotesdex stderr
 *  @cotesdex stdout
 *   @cotesdex content
 *   cat
 *   horse
 *   @cotesdex */

/* @cotesdex test
 *  @cotesdex cmdline
 *  -c yes --dog no
 *  @cotesdex
 *  @cotesdex status 0
 *  @cotesdex stderr
 *  @cotesdex stdout
 *   @cotesdex content
 *   cat
 *   horse
 *   @cotesdex */

/* @cotesdex test
 *  @cotesdex cmdline
 *  -c yes --dog=no
 *  @cotesdex
 *  @cotesdex status 0
 *  @cotesdex stderr
 *  @cotesdex stdout
 *   @cotesdex content
 *   cat
 *   horse
 *   @cotesdex */

/* @cotesdex test
 *  @cotesdex cmdline
 *  -d yes --horse yes
 *  @cotesdex
 *  @cotesdex status 0
 *  @cotesdex stderr
 *  @cotesdex stdout
 *   @cotesdex content
 *   cat
 *   dog
 *   horse
 *   @cotesdex */

/* @cotesdex test
 *  @cotesdex cmdline
 *  --dog yes --horse=yes
 *  @cotesdex
 *  @cotesdex status 0
 *  @cotesdex stderr
 *  @cotesdex stdout
 *   @cotesdex content
 *   cat
 *   dog
 *   horse
 *   @cotesdex */

/* @cotesdex test
 *  @cotesdex cmdline
 *  --dog=yes --horse yes
 *  @cotesdex
 *  @cotesdex status 0
 *  @cotesdex stderr
 *  @cotesdex stdout
 *   @cotesdex content
 *   cat
 *   dog
 *   horse
 *   @cotesdex */

/* @cotesdex test
 *  @cotesdex cmdline
 *  --dog=yes --horse=no
 *  @cotesdex
 *  @cotesdex status 0
 *  @cotesdex stderr
 *  @cotesdex stdout
 *   @cotesdex content
 *   cat
 *   dog
 *   @cotesdex */

/* @cotesdex test
 *  @cotesdex cmdline
 *  --dog=no --horse=no
 *  @cotesdex
 *  @cotesdex status 0
 *  @cotesdex stderr
 *  @cotesdex stdout
 *   @cotesdex content
 *   cat
 *   @cotesdex */

/* @cotesdex test
 *  @cotesdex cmdline
 *  -d no --horse yes
 *  @cotesdex
 *  @cotesdex status 0
 *  @cotesdex stderr
 *  @cotesdex stdout
 *   @cotesdex content
 *   cat
 *   horse
 *   @cotesdex */

/* @cotesdex test
 *  @cotesdex cmdline
 *  -c yes --dog=yes --horse=yes
 *  @cotesdex
 *  @cotesdex status 0
 *  @cotesdex stderr
 *  @cotesdex stdout
 *   @cotesdex content
 *   cat
 *   dog
 *   horse
 *   @cotesdex */

/* @cotesdex test
 *  @cotesdex cmdline
 *  -c no --dog yes --horse=no
 *  @cotesdex
 *  @cotesdex status 0
 *  @cotesdex stderr
 *  @cotesdex stdout
 *   @cotesdex content
 *   dog
 *   @cotesdex */

/* @cotesdex test
 *  @cotesdex cmdline
 *  -c yes --dog no --horse=yes
 *  @cotesdex
 *  @cotesdex status 0
 *  @cotesdex stderr
 *  @cotesdex stdout
 *   @cotesdex content
 *   cat
 *   horse
 *   @cotesdex */

/* @cotesdex hide
 * ... Then some situations GOP should not handle. In these cases stdout should
 * be left clean.
 * @cotesdex unhide */

/* @cotesdex test
 *  @cotesdex status 1
 *  @cotesdex cmdline
 *  -c yesy
 *  @cotesdex
 *  @cotesdex stdout */

/* @cotesdex test
 *  @cotesdex status 1
 *  @cotesdex cmdline
 *  -c nope
 *  @cotesdex
 *  @cotesdex stdout */

/* @cotesdex test
 *  @cotesdex status 1
 *  @cotesdex cmdline
 *  -d yeah
 *  @cotesdex
 *  @cotesdex stdout */

/* @cotesdex test
 *  @cotesdex status 1
 *  @cotesdex cmdline
 *  -d nope
 *  @cotesdex
 *  @cotesdex stdout */

/* @cotesdex test
 *  @cotesdex status 1
 *  @cotesdex cmdline
 *  --dog y
 *  @cotesdex
 *  @cotesdex stdout */

/* @cotesdex test
 *  @cotesdex status 1
 *  @cotesdex cmdline
 *  --dog n
 *  @cotesdex
 *  @cotesdex stdout */

/* @cotesdex test
 *  @cotesdex status 1
 *  @cotesdex cmdline
 *  --dog=YES
 *  @cotesdex
 *  @cotesdex stdout */

/* @cotesdex test
 *  @cotesdex status 1
 *  @cotesdex cmdline
 *  --dog=NO
 *  @cotesdex
 *  @cotesdex stdout */

/* @cotesdex test
 *  @cotesdex status 1
 *  @cotesdex cmdline
 *  --horse ya
 *  @cotesdex
 *  @cotesdex stdout */

/* @cotesdex test
 *  @cotesdex status 1
 *  @cotesdex cmdline
 *  --horse na
 *  @cotesdex
 *  @cotesdex stdout */

/* @cotesdex test
 *  @cotesdex status 1
 *  @cotesdex cmdline
 *  --horse=Y
 *  @cotesdex
 *  @cotesdex stdout */

/* @cotesdex test
 *  @cotesdex status 1
 *  @cotesdex cmdline
 *  --horse=N
 *  @cotesdex
 *  @cotesdex stdout */

/* @cotesdex test
 *  @cotesdex status 1
 *  @cotesdex cmdline
 *  -c
 *  @cotesdex
 *  @cotesdex stdout */

/* @cotesdex test
 *  @cotesdex status 1
 *  @cotesdex cmdline
 *  -d
 *  @cotesdex
 *  @cotesdex stdout */

/* @cotesdex test
 *  @cotesdex status 1
 *  @cotesdex cmdline
 *  --dog
 *  @cotesdex
 *  @cotesdex stdout */

/* @cotesdex test
 *  @cotesdex status 1
 *  @cotesdex cmdline
 *  --horse
 *  @cotesdex
 *  @cotesdex stdout */

/* @cotesdex test
 *  @cotesdex status 1
 *  @cotesdex cmdline
 *  -c -d
 *  @cotesdex
 *  @cotesdex stdout */

/* @cotesdex test
 *  @cotesdex status 1
 *  @cotesdex cmdline
 *  -cd
 *  @cotesdex
 *  @cotesdex stdout */

/* @cotesdex test
 *  @cotesdex status 1
 *  @cotesdex cmdline
 *  -c --dog
 *  @cotesdex
 *  @cotesdex stdout */

/* @cotesdex test
 *  @cotesdex status 1
 *  @cotesdex cmdline
 *  -c --horse
 *  @cotesdex
 *  @cotesdex stdout */

/* @cotesdex test
 *  @cotesdex status 1
 *  @cotesdex cmdline
 *  -d --horse
 *  @cotesdex
 *  @cotesdex stdout */

/* @cotesdex test
 *  @cotesdex status 1
 *  @cotesdex cmdline
 *  --dog --horse
 *  @cotesdex
 *  @cotesdex stdout */

/* @cotesdex test
 *  @cotesdex cmdline
 *  -cd no no
 *  @cotesdex
 *  @cotesdex status 1
 *  @cotesdex stdout */

/* @cotesdex test
 *  @cotesdex status 1
 *  @cotesdex cmdline
 *  -c -d --horse
 *  @cotesdex
 *  @cotesdex stdout */

/* @cotesdex test
 *  @cotesdex status 1
 *  @cotesdex cmdline
 *  -c --dog --horse
 *  @cotesdex
 *  @cotesdex stdout */

/* @cotesdex test
 *  @cotesdex cmdline
 *  -c --dog --horse yes yes yes
 *  @cotesdex
 *  @cotesdex status 1
 *  @cotesdex stdout */

/* Standard headers. */
#include <stdio.h> /* printf() */
#include <stdlib.h> /* EXIT_SUCCESS, EXIT_FAILURE */

#include <gop.h>

int
main(int argc, char ** const argv)
{
    /* This is the GOP context that will be used. */
    gop_t * gop;

    /* The variables GOP will fill in. */
    int cat = 1;
    int dog = 0;
    int horse = 1;

    const gop_option_t options[] = {
        {NULL, 'c', GOP_YESNO, &cat, NULL, "Print 'cat' [yes]", NULL},
        {"dog", 'd', GOP_YESNO, &dog, NULL, "Print 'dog' [no]", NULL},
        {"horse", '\0', GOP_YESNO, &horse, NULL, "Print 'horse' [yes]", NULL},
        GOP_TABLEEND
    };

    /* Get a new GOP context. */
    gop = gop_new();
    if (gop == NULL) {
        return EXIT_FAILURE;
    }

    gop_add_table(gop, NULL, options);
    gop_autohelp(gop);
    gop_parse(gop, &argc, argv);
    gop_destroy(gop);

    /* Print the requested animals. */
    if (cat) {
        printf("cat\n");
    }
    if (dog) {
        printf("dog\n");
    }
    if (horse) {
        printf("horse\n");
    }

    return EXIT_SUCCESS;
}
