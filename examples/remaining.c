/* @cotesdex license
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * @cotesdex */

/* @cotesdex title Unpaired arguments 1 */
/* @cotesdex description
 * This example shows how GOP handles non-option arguments. The example
 * program takes strings and possibly some options as input and prints
 * the strings to stdout.
 * 
 * Prerequisites:
 *  simple-parse
 * @cotesdex */

/* @cotesdex hide */

/* @cotesdex test
 *  @cotesdex status 0
 *  @cotesdex stderr
 *  @cotesdex stdout */

/* Some french writers from:
 * https://en.wikipedia.org/wiki/French_writer#1650-1699
 * @cotesdex test
 *  @cotesdex cmdline
 *  "de Caumont La Force" Dancourt Rollin Rousseau Voltaire Prevost
 *  @cotesdex status 0
 *  @cotesdex stderr
 *  @cotesdex stdout
 *   @cotesdex content
 *   de Caumont La Force
 *   Dancourt
 *   Rollin
 *   Rousseau
 *   Voltaire
 *   Prevost
 * @cotesdex */

/* Some Swedish Prime Ministers
 * https://en.wikipedia.org/wiki/List_of_Prime_Ministers_of_Sweden
 * @cotesdex test
 *  @cotesdex cmdline
 *  --numbering Hansson Erlander Palme Faalldin Ullsten
 *  @cotesdex stderr
 *  @cotesdex stdout
 *   @cotesdex content
 *   1: Hansson
 *   2: Erlander
 *   3: Palme
 *   4: Faalldin
 *   5: Ullsten
 * @cotesdex */

/* Some communist ideologies
 * https://en.wikipedia.org/wiki/List_of_communist_ideologies
 * @cotesdex
 * @cotesdex test
 *  @cotesdex cmdline
 *  --len Eurocommunism Leninism Luxemburgism Stalinism Trotskyism
 *  @cotesdex stderr
 *  @cotesdex stdout
 *   @cotesdex content
 *   len = 5
 *   Eurocommunism
 *   Leninism
 *   Luxemburgism
 *   Stalinism
 *   Trotskyism
 * @cotesdex */

/* Some species of Corydoras
 * https://en.wikipedia.org/wiki/List_of_Corydoras_species
 * @cotesdex test
 *  @cotesdex cmdline
 *  --len --sort --numbering
 *  bondi cochui elegans sterbai aenus pygmaeus xinguensis pastazensis
 *  griseus ambiacus julii adolfoi
 *  @cotesdex stderr
 *  @cotesdex stdout
 *   @cotesdex content
 *   len = 12
 *   1: adolfoi
 *   2: aenus
 *   3: ambiacus
 *   4: bondi
 *   5: cochui
 *   6: elegans
 *   7: griseus
 *   8: julii
 *   9: pastazensis
 *   10: pygmaeus
 *   11: sterbai
 *   12: xinguensis
 * @cotesdex */

/* @cotesdex unhide */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <gop.h>

static int
cmp(const void * s1, const void * s2)
{
    return strcmp(*(const char **)s1, *(const char **)s2);
}

int
main(int argc, char ** const argv)
{
    int print_len = 0;
    int numbering = 0;
    int sort      = 0;

    /* This is option table that is going to be used. */
    const gop_option_t options[] = {
        {"len",       '\0', GOP_NONE, &print_len, NULL, NULL, NULL},
        {"numbering", '\0', GOP_NONE, &numbering, NULL, NULL, NULL},
        {"sort",      '\0', GOP_NONE, &sort,      NULL, NULL, NULL},
        GOP_TABLEEND
    };

    gop_t * const gop = gop_new();
    if (gop == NULL) {
        return EXIT_FAILURE;
    }

    gop_add_table(gop, NULL, options);
    gop_parse(gop, &argc, argv);
    gop_destroy(gop);

    if (print_len) {
        printf("len = %d\n", argc - 1);
    }

    if (sort) {
        qsort(argv + 1, (size_t)(argc - 1), sizeof(char *), &cmp);
    }

    for (int i = 1; i < argc; ++i) {
        if (numbering) {
            printf("%d: ", i);
        }
        puts(argv[i]);
    }

    return EXIT_SUCCESS;
}
