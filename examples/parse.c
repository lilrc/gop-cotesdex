/* @cotesdex license
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * @cotesdex */

/* @cotesdex title A simple GOP program */
/* @cotesdex description
 * This example demonstrates more properties of the gop_parse function.
 * @cotesdex */

/* @cotesdex test
 *  @cotesdex cmdline
 *  @cotesdex
 *  @cotesdex status 0
 *  @cotesdex stderr
 *  @cotesdex stdout
 *   @cotesdex content
 *   ret = 1
 *   argc = 1
 *   argv[1] = NULL
 *   @cotesdex */

/* @cotesdex test
 *  @cotesdex cmdline
 *  --aphrodite=godess
 *  @cotesdex
 *  @cotesdex status 0
 *  @cotesdex stderr
 *  @cotesdex stdout
 *   @cotesdex content
 *   ret = 1
 *   argc = 1
 *   argv[1] = NULL
 *   aphrodite = godess
 *   @cotesdex */

/* @cotesdex test
 *  @cotesdex cmdline
 *  --zeus=god
 *  @cotesdex
 *  @cotesdex status 0
 *  @cotesdex stderr
 *  @cotesdex stdout
 *   @cotesdex content
 *   zeus = god
 *   ret = 1
 *   argc = 1
 *   argv[1] = NULL
 *   @cotesdex */

/* @cotesdex test
 *  @cotesdex cmdline
 *  --return
 *  @cotesdex
 *  @cotesdex status 0
 *  @cotesdex stderr
 *  @cotesdex stdout
 *   @cotesdex content
 *   ret = 1
 *   argc = 2
 *   argv[1] = --return
 *   argv[2] = NULL
 *   @cotesdex */

/* @cotesdex test
 *  @cotesdex cmdline
 *  --unknown
 *  @cotesdex
 *  @cotesdex status 1
 *  @cotesdex stderr
 *  @cotesdex stdout
 *   @cotesdex content
 *   error occurred
 *   ret = -2
 *   argc = 2
 *   argv[1] = --unknown
 *   argv[2] = NULL
 *   @cotesdex */

/* @cotesdex test
 *  @cotesdex cmdline
 *  --zeus=god --unknown
 *  @cotesdex
 *  @cotesdex status 1
 *  @cotesdex stderr
 *  @cotesdex stdout
 *   @cotesdex content
 *   zeus = god
 *   error occurred
 *   ret = -2
 *   argc = 2
 *   argv[1] = --unknown
 *   argv[2] = NULL
 *   @cotesdex */

/* @cotesdex test
 *  @cotesdex cmdline
 *  --unknown --zeus=god
 *  @cotesdex
 *  @cotesdex status 1
 *  @cotesdex stderr
 *  @cotesdex stdout
 *   @cotesdex content
 *   zeus = god
 *   error occurred
 *   ret = -2
 *   argc = 2
 *   argv[1] = --unknown
 *   argv[2] = NULL
 *   @cotesdex */

/* @cotesdex test
 *  @cotesdex cmdline
 *  --aphrodite=godess --unknown
 *  @cotesdex
 *  @cotesdex status 1
 *  @cotesdex stderr
 *  @cotesdex stdout
 *   @cotesdex content
 *   error occurred
 *   ret = -2
 *   argc = 2
 *   argv[1] = --unknown
 *   argv[2] = NULL
 *   aphrodite = godess
 *   @cotesdex */

/* @cotesdex test
 *  @cotesdex cmdline
 *  --unknown --aphrodite=godess
 *  @cotesdex
 *  @cotesdex status 1
 *  @cotesdex stderr
 *  @cotesdex stdout
 *   @cotesdex content
 *   error occurred
 *   ret = -2
 *   argc = 2
 *   argv[1] = --unknown
 *   argv[2] = NULL
 *   aphrodite = godess
 *   @cotesdex */

/* @cotesdex test
 *  @cotesdex cmdline
 *  --aphrodite=godess --unknown --dionysus
 *  @cotesdex
 *  @cotesdex status 1
 *  @cotesdex stderr
 *  @cotesdex stdout
 *   @cotesdex content
 *   error occurred
 *   ret = -2
 *   argc = 2
 *   argv[1] = --unknown
 *   argv[2] = NULL
 *   aphrodite = godess
 *   dionysus
 *   @cotesdex */

/* @cotesdex test
 *  @cotesdex cmdline
 *  --zeus god --return
 *  @cotesdex
 *  @cotesdex status 0
 *  @cotesdex stderr
 *  @cotesdex stdout
 *   @cotesdex content
 *   zeus = god
 *   ret = 1
 *   argc = 2
 *   argv[1] = --return
 *   argv[2] = NULL
 *   @cotesdex */

/* @cotesdex test
 *  @cotesdex cmdline
 *  --return --zeus god
 *  @cotesdex
 *  @cotesdex status 0
 *  @cotesdex stderr
 *  @cotesdex stdout
 *   @cotesdex content
 *   ret = 1
 *   argc = 4
 *   argv[1] = --return
 *   argv[2] = --zeus
 *   argv[3] = god
 *   argv[4] = NULL
 *   @cotesdex */

/* @cotesdex test
 *  @cotesdex cmdline
 *  --return --aphrodite godess
 *  @cotesdex
 *  @cotesdex status 0
 *  @cotesdex stderr
 *  @cotesdex stdout
 *   @cotesdex content
 *   ret = 1
 *   argc = 4
 *   argv[1] = --return
 *   argv[2] = --aphrodite
 *   argv[3] = godess
 *   argv[4] = NULL
 *   @cotesdex */

/* @cotesdex test
 *  @cotesdex cmdline
 *  --unknown --return
 *  @cotesdex
 *  @cotesdex status 1
 *  @cotesdex stderr
 *  @cotesdex stdout
 *   @cotesdex content
 *   error occurred
 *   ret = -2
 *   argc = 3
 *   argv[1] = --unknown
 *   argv[2] = --return
 *   argv[3] = NULL
 *   @cotesdex */

/* @cotesdex test
 *  @cotesdex cmdline
 *  -r
 *  @cotesdex
 *  @cotesdex status 0
 *  @cotesdex stderr
 *  @cotesdex stdout
 *   @cotesdex content
 *   ret = 1
 *   argc = 2
 *   argv[1] = -r
 *   argv[2] = NULL
 *   @cotesdex */

/* @cotesdex test
 *  @cotesdex cmdline
 *  -u -r
 *  @cotesdex
 *  @cotesdex status 1
 *  @cotesdex stderr
 *  @cotesdex stdout
 *   @cotesdex content
 *   error occurred
 *   ret = -2
 *   argc = 3
 *   argv[1] = -u
 *   argv[2] = -r
 *   argv[3] = NULL
 *   @cotesdex */

/* @cotesdex test
 *  @cotesdex cmdline
 *  -ur
 *  @cotesdex
 *  @cotesdex status 1
 *  @cotesdex stderr
 *  @cotesdex stdout
 *   @cotesdex content
 *   error occurred
 *   ret = -1
 *   argc = 2
 *   argv[1] = -ur
 *   argv[2] = NULL
 *   @cotesdex */

/* @cotesdex test
 *  @cotesdex cmdline
 *  -ru
 *  @cotesdex
 *  @cotesdex status 0
 *  @cotesdex stderr
 *  @cotesdex stdout
 *   @cotesdex content
 *   ret = 1
 *   argc = 2
 *   argv[1] = -ru
 *   argv[2] = NULL
 *   @cotesdex */

/* @cotesdex test
 *  @cotesdex cmdline
 *  -dur
 *  @cotesdex
 *  @cotesdex status 1
 *  @cotesdex stderr
 *  @cotesdex stdout
 *   @cotesdex content
 *   error occurred
 *   ret = -1
 *   argc = 2
 *   argv[1] = -ur
 *   argv[2] = NULL
 *   dionysus
 *   @cotesdex */

/* @cotesdex test
 *  @cotesdex cmdline
 *  -zur
 *  @cotesdex
 *  @cotesdex status 1
 *  @cotesdex stderr
 *  @cotesdex stdout
 *   @cotesdex content
 *   ouch
 *   error occurred
 *   ret = -1
 *   argc = 2
 *   argv[1] = -zur
 *   argv[2] = NULL
 *   @cotesdex */

/* @cotesdex test
 *  @cotesdex cmdline
 *  -zdr
 *  @cotesdex
 *  @cotesdex status 1
 *  @cotesdex stderr
 *  @cotesdex stdout
 *   @cotesdex content
 *   ouch
 *   error occurred
 *   ret = -1
 *   argc = 2
 *   argv[1] = -zr
 *   argv[2] = NULL
 *   dionysus
 *   @cotesdex */

/* @cotesdex test
 *  @cotesdex cmdline
 *  -rz
 *  @cotesdex
 *  @cotesdex status 0
 *  @cotesdex stderr
 *  @cotesdex stdout
 *   @cotesdex content
 *   ret = 1
 *   argc = 2
 *   argv[1] = -rz
 *   argv[2] = NULL
 *   @cotesdex */


#include <stdio.h>

#include <gop.h>

static int error = 0;

/* There is no reason to initialise this since it will be set by GOP when it is
 * found. */
static char * zeus;

static gop_return_t
cb_error(gop_t * const gop)
{
    error = 1;
    switch (gop_get_error(gop)) {
        case GOP_ERROR_COMPSHRTOPTEXPARG:
            puts("ouch");
            /* fallthrough */
        case GOP_ERROR_UNKLOPT:
            /* fallthrough */
        case GOP_ERROR_UNKSOPT:
            return GOP_DO_CONTINUE;
        default:
            break;
    }
    return GOP_DO_ERROR;
}

static gop_return_t
cb_return(gop_t * const gop)
{
    return GOP_DO_RETURN;
}

static gop_return_t
cb_zeus(gop_t * const gop)
{
    printf("zeus = %s\n", zeus);
    return GOP_DO_CONTINUE;
}

int
main(int argc, char ** const argv)
{
    int exit_status = 0;

    char * aphrodite = NULL;
    int dionysus = 0;

    const gop_option_t options[] = {
        {"aphrodite", 'a', GOP_STRING, &aphrodite, NULL, NULL, NULL},
        {"dionysus", 'd', GOP_NONE, &dionysus, NULL, NULL, NULL},
        {"zeus", 'z', GOP_STRING, &zeus, &cb_zeus, NULL, NULL},
        {"return", 'r', GOP_NONE, NULL, &cb_return, NULL, NULL},
        GOP_TABLEEND
    };

    gop_t * const gop = gop_new();
    if (gop == NULL) {
        return 1;
    }

    gop_add_table(gop, NULL, options);
    gop_aterror(gop, &cb_error);
    int ret = gop_parse(gop, &argc, argv);
    gop_destroy(gop);

    if (error) {
        puts("error occurred");
        exit_status = 1;
    }

    printf("ret = %d\n", ret);
    printf("argc = %d\n", argc);
    for (int i = 1; i <= argc; ++i) {
        if (argv[i] != NULL) {
            printf("argv[%d] = %s\n", i, argv[i]);
        } else {
            printf("argv[%d] = NULL\n", i);
        }
    }

    if (aphrodite != NULL) {
        printf("aphrodite = %s\n", aphrodite);
    }

    if (dionysus != 0) {
        puts("dionysus");
    }

    return exit_status;
}
