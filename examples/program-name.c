/* @cotesdex license
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * @cotesdex */

/* @cotesdex title How GOP handles the program name */
/* @cotesdex description
 * Prerequisites:
 *  autohelp
 *  prevent-exit
 *  simple-parse
 * 
 * This example shows how the program name is handled by GOP in
 * different cases.
 * 
 * The example also includes a mechanism that will make GOP print out an
 * error on request from the command line.
 * 
 * . Create a GOP context using +gop_new()+.
 * . Feed the GOP context with an option table using +gop_add_table()+.
 * . Add the automatic help output option table using +gop_autohelp()+.
 * . Parse the options using +gop_parse_options()+.
 * . Print the result.
 * . Destroy the context using +gop_destroy()+.
 * @cotesdex */

/* @cotesdex test
 *  @cotesdex cmdline
 *  --error
 *  @cotesdex stderr
 *   @cotesdex content
 *   program-name: invalid return value: 123
 *  @cotesdex stdout
 * @cotesdex */

/* @cotesdex test
 *  @cotesdex cmdline
 *  --help
 *  @cotesdex stdout
 *   @cotesdex content
 *   Usage: program-name [OPTION...]
 *   
 *         --error        Trigger an error
 *         --name=NAME    Set the name of the program
 *   
 *   Help options:
 *     -?, --help         Show this help message
 *         --usage        Display brief usage message
 *   @cotesdex */

/* @cotesdex test
 *  @cotesdex cmdline
 *  --usage
 *  @cotesdex stdout
 *   @cotesdex content
 *   Usage: program-name [-?] [--error] [--name=NAME] [-?|--help] [--usage]
 *   @cotesdex */

/* @cotesdex test
 *  @cotesdex cmdline
 *  --name=hello --error
 *  @cotesdex stderr
 *   @cotesdex content
 *   hello: invalid return value: 123
 *  @cotesdex stdout
 * @cotesdex */

/* @cotesdex test
 *  @cotesdex cmdline
 *  --name=yeah --error
 *  @cotesdex stderr
 *   @cotesdex content
 *   yeah: invalid return value: 123
 *  @cotesdex stdout
 * @cotesdex */

/* @cotesdex test
 *  @cotesdex cmdline
 *  --name=hi --help
 *  @cotesdex stdout
 *   @cotesdex content
 *   Usage: hi [OPTION...]
 *   
 *         --error        Trigger an error
 *         --name=NAME    Set the name of the program
 *   
 *   Help options:
 *     -?, --help         Show this help message
 *         --usage        Display brief usage message
 *   @cotesdex */

/* @cotesdex test
 *  @cotesdex cmdline
 *  --name="Charles Dickens" --help
 *  @cotesdex stdout
 *   @cotesdex content
 *   Usage: Charles Dickens [OPTION...]
 *   
 *         --error        Trigger an error
 *         --name=NAME    Set the name of the program
 *   
 *   Help options:
 *     -?, --help         Show this help message
 *         --usage        Display brief usage message
 *   @cotesdex */

/* @cotesdex test
 *  @cotesdex cmdline
 *  --name=yo --usage
 *  @cotesdex stdout
 *   @cotesdex content
 *   Usage: yo [-?] [--error] [--name=NAME] [-?|--help] [--usage]
 *   @cotesdex */

/* @cotesdex test
 *  @cotesdex cmdline
 *  --name="Alexandre Dumas" --usage
 *  @cotesdex stdout
 *   @cotesdex content
 *   Usage: Alexandre Dumas [-?] [--error] [--name=NAME] [-?|--help] [--usage]
 *   @cotesdex */

/* Standard headers. */
#include <stdlib.h> /* EXIT_SUCCESS, EXIT_FAILURE */

#include <gop.h>

/* This is the program name that will be used. If it is NULL, GOP will
 * guess the program name from the first argument if it can, otherwise
 * it will use the string "gop". */
static char * program_name = NULL;

/* This function will be called when the --name option is parsed.
 * A callback is needed to make it possible to change the program name
 * before help or usage is printed, otherwise the --name option would
 * have no effect. */
static gop_return_t
program_name_callback(gop_t * const gop)
{
    gop_set_program_name(gop, program_name);
    return GOP_DO_CONTINUE;
}

/* This callback is used to make GOP print and error. It works by returning an
 * invalid return value. You should not attempt to do something this in a
 * program that should work! */
static gop_return_t
error_callback(gop_t * const gop)
{
    /* This should be enough invalid. */
    return 123;
}

int
main(int argc, char ** const argv)
{
    const gop_option_t options[] = {
        {"error", '\0', GOP_NONE, NULL, &error_callback,
            "Trigger an error", NULL},
        {"name", '\0', GOP_STRING, &program_name,
            &program_name_callback, "Set the name of the program",
            "NAME"},
        GOP_TABLEEND
    };

    gop_t * const gop = gop_new();
    if (gop == NULL) {
        return EXIT_FAILURE;
    }

    gop_add_table(gop, NULL, options);
    gop_autohelp(gop);
    gop_parse(gop, &argc, argv);
    gop_destroy(gop);

    return EXIT_SUCCESS;
}
