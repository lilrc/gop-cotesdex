/* @cotesdex license
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * @cotesdex */

/* @cotesdex title An exampe of the autohelp table */
/* @cotesdex description
 * Prerequisites:
 *  autohelp
 *  simple-parse
 * 
 * This example demonstrates how to prevent any help printing functions'
 * from exiting the program. +
 * The example does the following:
 * 
 * . Create a GOP context using +gop_new()+.
 * . Register an atexit fuinction that will be called before GOP
 *   attempts to exit. Registering is done with +gop_atexit()+.
 * . Add the automatic help output option table using +gop_autohelp()+.
 * . Parse the options using +gop_parse_options()+.
 * . Print the result.
 * . Destroy the context using +gop_destroy()+.
 * @cotesdex */

/* @cotesdex test
 *  @cotesdex status 0
 *  @cotesdex stderr
 *  @cotesdex stdout
 *   @cotesdex content
 *   GOP did not exit.
 *   @cotesdex */

/* @cotesdex test
 *  @cotesdex cmdline
 *  --help
 *  @cotesdex 
 *  @cotesdex status 0
 *  @cotesdex stdout
 *   @cotesdex content
 *   Usage: prevent-exit [OPTION...]
 *   
 *   Help options:
 *     -?, --help     Show this help message
 *         --usage    Display brief usage message
 *   GOP did not exit.
 *   @cotesdex */

/* @cotesdex test
 *  @cotesdex cmdline
 *  --help --help
 *  @cotesdex 
 *  @cotesdex status 0
 *  @cotesdex stdout
 *   @cotesdex content
 *   Usage: prevent-exit [OPTION...]
 *   
 *   Help options:
 *     -?, --help     Show this help message
 *         --usage    Display brief usage message
 *   Usage: prevent-exit [OPTION...]
 *   
 *   Help options:
 *     -?, --help     Show this help message
 *         --usage    Display brief usage message
 *   GOP did not exit.
 *   @cotesdex */

/* @cotesdex test
 *  @cotesdex cmdline
 *  --usage
 *  @cotesdex
 *  @cotesdex status 0
 *  @cotesdex stdout
 *   @cotesdex content
 *   Usage: prevent-exit [-?] [-?|--help] [--usage]
 *   GOP did not exit.
 *   @cotesdex */

/* @cotesdex test
 *  @cotesdex cmdline
 *  --help --usage
 *  @cotesdex
 *  @cotesdex status 0
 *  @cotesdex stdout
 *   @cotesdex content
 *   Usage: prevent-exit [OPTION...]
 *   
 *   Help options:
 *     -?, --help     Show this help message
 *         --usage    Display brief usage message
 *   Usage: prevent-exit [-?] [-?|--help] [--usage]
 *   GOP did not exit.
 *   @cotesdex */

/* Standard headers. */
#include <assert.h> /* assert() */
#include <stdio.h> /* printf() */
#include <stdlib.h> /* EXIT_SUCCESS, EXIT_FAILURE */

#include <gop.h>

/* This is the function that will be sent to gop_atexit() and will thus be
 * called before GOP attempts to exit the program. Note that there is a void *
 * argument to the function. That is the same pointer that was given to
 * gop_atexit(). */
static gop_return_t
my_atexit_func(gop_t * const gop, void * argument)
{
    /* NULL was given to gop_atexit() so assert that NULL is sent to this
     * function. This is definitely not necessary; it is just here to show that
     * it really is NULL. */
    assert(argument == NULL);

    /* An atexit function can do other arbitrary things such as closing open
     * files before exiting. */

    /* The return value of this function tells GOP what to do next.
     * There are a few valid return values. The most common are;
     *  GOP_DO_CONTINE - This return value tells GOP to continue parsing
     *                   options (or whatever it did when it decided to
     *                   exit) and abort the exiting.
     * GOP_DO_EXIT     - This return value tells GOP to exit.
     * GOP_DO_RETURN   - This return value tells GOP to return control
     *                   to the program aborting exit. */
    return GOP_DO_CONTINUE;
}

int
main(int argc, char ** const argv)
{
    /* The return value. It will be changed to 1 in the error goto. */
    int exit_status = EXIT_SUCCESS;

    /* A value to store the return value of the GOP functions. Used to check
     * that everything is OK. */
    int ret;

    /* Get a new GOP context. */
    gop_t * const gop = gop_new();
    if (gop == NULL) {
        goto error;
    }

    /* This tells GOP that it should call my_atexit_func before attempting to
     * exit. The third argument (NULL in this example) is a pointer that will be
     * sent to the atexit function. */
    gop_atexit(gop, &my_atexit_func, NULL);

    /* Add the autohelp option table. */
    ret = gop_autohelp(gop);
    if (ret) {
        goto error;
    }

    /* Parse the options. */
    ret = gop_parse(gop, &argc, argv);
    if (ret <= 0) {
        /* gop_parse_options() will return non-positive. */
        goto error;
    }

    /* This is just here to show that the program did not exit in the call to
     * gop_parse_options(). */
    printf("GOP did not exit.\n");

    /* If an error occurs the code will jump into this clause. It will then set
     * the return value to indicate failure. */
    if (0) {
    error:
        exit_status = EXIT_FAILURE;
    }

    /* Destroy the GOP context. */
    gop_destroy(gop);

    return exit_status;
}
