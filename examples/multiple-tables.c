/* @cotesdex license
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * @cotesdex */

/* @cotesdex title A program with multiple option tables */
/* @cotesdex description
 * Prerequisites:
 *  simple-parse
 *  autohelp 
 * 
 * This example shows how GOP handles many option tables. It does the
 * following:
 * 
 * . Create a GOP context using +gop_new()+.
 * . Feed the GOP context with a few option tables using +gop_add_table()+.
 * . Parse the options using +gop_parse_options()+.
 * . Print the result.
 * . Destroy the context using +gop_destroy()+.
 * @cotesdex */

/* @cotesdex hide
 * First tests that GOP should handle... Standard error should be clean in
 * almost all cases.
 * @cotesdex unhide */

/* @cotesdex test
 *  @cotesdex status 0
 *  @cotesdex stderr
 *  @cotesdex stdout */

/* @cotesdex test
 *  @cotesdex cmdline
 *  --help
 *  @cotesdex
 *  @cotesdex status 0
 *  @cotesdex stdout
 *   @cotesdex content
 *   Usage: multiple-tables [OPTION...]
 *   
 *   Table 1
 *         --string1=<string1>                  Print a string
 *         --string2=<string2>                  Print another string
 *   
 *   Table 2
 *     -s, --short=<short>                      Print a short
 *         --unsigned-short=<unsigned-short>    Print an unsigned short
 *   
 *   Table 3
 *     -d, --double=<double>                    Print a double
 *     -f, --float=<float>                      Print a float
 *   
 *   Table 4
 *     -i, --integer=<integer>                  Print an integer
 *         --long-integer=<long-integer>        Print a long integer
 *   
 *   Table 5
 *         --flag                               Print a flag message
 *   
 *   Help options:
 *     -?, --help                               Show this help message
 *         --usage                              Display brief usage message
 *   @cotesdex */

/* @cotesdex test
 *  @cotesdex cmdline
 *  --string1=Hello --integer=75
 *  @cotesdex
 *  @cotesdex status 0
 *  @cotesdex stderr
 *  @cotesdex stdout
 *   @cotesdex content
 *   string1 = Hello
 *   integer = 75
 *   @cotesdex */

/* @cotesdex test
 *  @cotesdex cmdline
 *  --string1=man --string2=woman
 *  @cotesdex
 *  @cotesdex status 0
 *  @cotesdex stderr
 *  @cotesdex stdout
 *   @cotesdex content
 *   string1 = man
 *   string2 = woman
 *   @cotesdex */

/* @cotesdex test
 *  @cotesdex cmdline
 *  --string1 "Strange numbers" -i 39 -d 26.397124
 *  @cotesdex
 *  @cotesdex status 0
 *  @cotesdex stderr
 *  @cotesdex stdout
 *   @cotesdex content 
 *   string1 = Strange numbers
 *   double = 26.397124
 *   integer = 39
 *   @cotesdex */

/* @cotesdex test
 *  @cotesdex cmdline
 *  --string2 "One can never test too much" --short 30012 --unsigned-short 65530
 *  @cotesdex
 *  @cotesdex status 0
 *  @cotesdex stderr
 *  @cotesdex stdout
 *   @cotesdex content
 *   string2 = One can never test too much
 *   short = 30012
 *   unsigned-short = 65530
 *   @cotesdex */

/* @cotesdex test
 *  @cotesdex cmdline
 *  --flag
 *  @cotesdex
 *  @cotesdex status 0
 *  @cotesdex stderr
 *  @cotesdex stdout
 *   @cotesdex content
 *   --flag was given.
 *   @cotesdex */

/* @cotesdex test
 *  @cotesdex cmdline
 *  --flag --float 10 --long-integer 9836713213
 *  @cotesdex
 *  @cotesdex status 0
 *  @cotesdex stderr
 *  @cotesdex stdout
 *   @cotesdex content
 *   float = 10.000000
 *   long-integer = 9836713213
 *   --flag was given.
 *   @cotesdex */

/* @cotesdex hide
 * ... Then some situations GOP should not handle. In these cases stdout should
 * be left clean.
 * @cotesdex unhide */

/* @cotesdex test
 *  @cotesdex status 0
 *  @cotesdex cmdline
 *  --string1 --string2
 *  @cotesdex
 *  @cotesdex stdout
 *   @cotesdex content
 *   string1 = --string2
 *   @cotesdex */

/* @cotesdex test
 *  @cotesdex status 1
 *  @cotesdex cmdline
 *  --string1 "I like cats" -i
 *  @cotesdex
 *  @cotesdex stdout */

/* @cotesdex test
 *  @cotesdex status 1
 *  @cotesdex cmdline
 *  --float --double
 *  @cotesdex
 *  @cotesdex stdout */

/* @cotesdex test
 *  @cotesdex status 1
 *  @cotesdex cmdline
 *  -dfis
 *  @cotesdex
 *  @cotesdex stdout
 * @cotesdex */

/* Standard headers. */
#include <limits.h> /* USHRT_MAX */
#include <math.h> /* INFINITY */
#include <stdio.h> /* fprintf(), printf() */
#include <stdlib.h> /* atexit(), EXIT_SUCCESS, EXIT_FAILURE */

#include <gop.h>

int
main(int argc, char ** const argv)
{
    /* This is the GOP context that will be used. */
    gop_t * gop;

    /* The variables GOP will fill in. */
    char * string1 = NULL;
    char * string2 = NULL;
    double dbl = INFINITY;
    float flt = INFINITY;
    int flag = 0;
    int integer = -1;
    long int long_integer = -1;
    short shrt = -1;
    unsigned ushrt = USHRT_MAX;

    const gop_option_t table1[] = {
        {"string1", '\0', GOP_STRING, &string1, NULL,
            "Print a string", "<string1>"},
        {"string2", '\0', GOP_STRING, &string2, NULL,
            "Print another string", "<string2>"},
        GOP_TABLEEND
    };

    const gop_option_t table2[] = {
        {"short", 's', GOP_SHORT, &shrt, NULL,
            "Print a short", "<short>"},
        {"unsigned-short", '\0', GOP_UNSIGNED_SHORT, &ushrt, NULL,
            "Print an unsigned short", "<unsigned-short>"},
        GOP_TABLEEND
    };

    const gop_option_t table3[] = {
        {"double", 'd', GOP_DOUBLE, &dbl, NULL,
            "Print a double", "<double>"},
        {"float", 'f', GOP_FLOAT, &flt, NULL,
            "Print a float", "<float>"},
        GOP_TABLEEND
    };

    const gop_option_t table4[] = {
        {"integer", 'i', GOP_INT, &integer, NULL,
            "Print an integer", "<integer>"},
        {"long-integer", '\0', GOP_LONG_INT, &long_integer, NULL,
            "Print a long integer", "<long-integer>"},
        GOP_TABLEEND
    };

    const gop_option_t table5[] = {
        /* GOP sets flag to be 1 if this option is given. */
        {"flag", '\0', GOP_NONE, &flag, NULL, "Print a flag message",
            NULL},
        GOP_TABLEEND
    };

    /* Get a new GOP context. */
    gop = gop_new();
    if (gop == NULL) {
        return EXIT_FAILURE;
    }

    /* Add the option tables. */
    gop_add_table(gop, "Table 1", table1);
    gop_add_table(gop, "Table 2", table2);
    gop_add_table(gop, "Table 3", table3);
    gop_add_table(gop, "Table 4", table4);
    gop_add_table(gop, "Table 5", table5);
    gop_autohelp(gop);

    /* Parse the options. */
    gop_parse(gop, &argc, argv);

    /* The GOP context is not needed anymore so destroy it. */
    gop_destroy(gop);

    /* Print the variables that are not at the default value. */
    if (string1 != NULL) {
        printf("string1 = %s\n", string1);
    }
    if (string2 != NULL) {
        printf("string2 = %s\n", string2);
    }
    if (shrt != -1) {
        printf("short = %d\n", shrt);
    }
    if (ushrt != USHRT_MAX) {
        printf("unsigned-short = %u\n", ushrt);
    }
    if (dbl != INFINITY) {
        printf("double = %f\n", dbl);
    }
    if (flt != INFINITY) {
        printf("float = %f\n", flt);
    }
    if (integer != -1) {
        printf("integer = %d\n", integer);
    }
    if (long_integer != -1) {
        printf("long-integer = %ld\n", long_integer);
    }
    if (flag) {
        printf("--flag was given.\n");
    }

    return EXIT_SUCCESS;
}
