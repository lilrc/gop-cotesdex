/* @cotesdex license
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * @cotesdex */

/* @cotesdex title An exampe of the autohelp table */
/* @cotesdex description
 * Prerequisites:
 *  simple-parse
 * 
 * This example shows how the autohelp table can be used to generate
 * automated help output. It does the following:
 * 
 * . Create a GOP context using +gop_new()+.
 * . Feed the GOP context with an option table using +gop_add_table()+.
 * . Add the automatic help output option table using +gop_autohelp()+.
 * . Parse the options using +gop_parse_options()+.
 * . Print the result.
 * . Destroy the context using +gop_destroy()+.
 * @cotesdex */

/* @cotesdex hide
 * First the tests that GOP should handle... Standard error should be clean in
 * almost all cases.
 * @cotesdex unhide */

/* @cotesdex test
 *  @cotesdex status 0
 *  @cotesdex stderr
 *  @cotesdex stdout */

/* @cotesdex test
 *  @cotesdex cmdline
 *  --help
 *  @cotesdex
 *  @cotesdex status 0
 *  @cotesdex stdout
 *   @cotesdex content
 *   Usage: autohelp [OPTION...]
 *   
 *     -d, --double=<double>                    Print a double
 *     -f, --float=<float>                      Print a float
 *     -i, --integer=<integer>                  Print an integer
 *     -l, --long-integer=<long-integer>        Print a long integer
 *     -h, --short=<short>                      Print a short
 *     -u, --unsigned-short=<unsigned-short>    Print an unsigned short
 *   
 *   Help options:
 *     -?, --help                               Show this help message
 *         --usage                              Display brief usage message
 *   @cotesdex */

/* @cotesdex test
 *  @cotesdex cmdline
 *  -?
 *  @cotesdex
 *  @cotesdex status 0
 *  @cotesdex stdout
 *   @cotesdex content
 *   Usage: autohelp [OPTION...]
 *   
 *     -d, --double=<double>                    Print a double
 *     -f, --float=<float>                      Print a float
 *     -i, --integer=<integer>                  Print an integer
 *     -l, --long-integer=<long-integer>        Print a long integer
 *     -h, --short=<short>                      Print a short
 *     -u, --unsigned-short=<unsigned-short>    Print an unsigned short
 *   
 *   Help options:
 *     -?, --help                               Show this help message
 *         --usage                              Display brief usage message
 *   @cotesdex */

/* @cotesdex test
 *  @cotesdex cmdline
 *  --usage
 *  @cotesdex
 *  @cotesdex status 0
 *  @cotesdex stdout
 *   @cotesdex content
 *   Usage: autohelp [-?] [-d|--double=<double>] [-f|--float=<float>]
 *       [-i|--integer=<integer>] [-l|--long-integer=<long-integer>]
 *       [-h|--short=<short>] [-u|--unsigned-short=<unsigned-short>] [-?|--help]
 *       [--usage]
 *   @cotesdex */

/* @cotesdex hide
 * The program should exit when --help is found.
 * @cotesdex unhide
 * @cotesdex test
 *  @cotesdex cmdline
 *  --integer=2 --float=4.3 --help -d 23.112
 *  @cotesdex stdout
 *   @cotesdex content
 *   Usage: autohelp [OPTION...]
 *   
 *     -d, --double=<double>                    Print a double
 *     -f, --float=<float>                      Print a float
 *     -i, --integer=<integer>                  Print an integer
 *     -l, --long-integer=<long-integer>        Print a long integer
 *     -h, --short=<short>                      Print a short
 *     -u, --unsigned-short=<unsigned-short>    Print an unsigned short
 *   
 *   Help options:
 *     -?, --help                               Show this help message
 *         --usage                              Display brief usage message
 *   @cotesdex */

/* @cotesdex test
 *  @cotesdex cmdline
 *  --integer=10
 *  @cotesdex
 *  @cotesdex status 0
 *  @cotesdex stderr
 *  @cotesdex stdout
 *   @cotesdex content
 *   integer = 10
 *   @cotesdex */

/* @cotesdex test
 *  @cotesdex cmdline
 *  -i 39 -d 26.397124
 *  @cotesdex
 *  @cotesdex status 0
 *  @cotesdex stderr
 *  @cotesdex stdout
 *   @cotesdex content
 *   double = 26.397124
 *   integer = 39
 *   @cotesdex */

/* @cotesdex test
 *  @cotesdex cmdline
 *  -h 30012 --unsigned-short 65530
 *  @cotesdex
 *  @cotesdex status 0
 *  @cotesdex stderr
 *  @cotesdex stdout
 *   @cotesdex content
 *   short = 30012
 *   unsigned-short = 65530
 *   @cotesdex */

/* @cotesdex test
 *  @cotesdex cmdline
 *  --float 10 --long-integer 1836715313
 *  @cotesdex
 *  @cotesdex status 0
 *  @cotesdex stderr
 *  @cotesdex stdout
 *   @cotesdex content
 *   float = 10.000000
 *   long-integer = 1836715313
 *   @cotesdex */

/* @cotesdex hide
 * ... Then some situations GOP should not handle. In these cases stdout should
 * be left clean.
 * @cotesdex unhide */

/* @cotesdex test
 *  @cotesdex status 1
 *  @cotesdex cmdline
 *  --float --double
 *  @cotesdex
 *  @cotesdex stdout */

/* @cotesdex test
 *  @cotesdex status 1
 *  @cotesdex cmdline
 *  -uh
 *  @cotesdex
 *  @cotesdex stdout
 * @cotesdex */

/* Standard headers. */
#include <limits.h> /* USHRT_MAX */
#include <math.h> /* INFINITY */
#include <stdio.h> /* printf() */
#include <stdlib.h> /* EXIT_SUCCESS, EXIT_FAILURE */

#include <gop.h>

int
main(int argc, char ** const argv)
{
    /* This is the GOP context that will be used. */
    gop_t * gop;

    /* The variables GOP will fill in. */
    double dbl = INFINITY;
    float flt = INFINITY;
    int integer = -1;
    long int long_integer = -1;
    short shrt = -1;
    unsigned ushrt = USHRT_MAX;

    const gop_option_t options[] = {
        {"double", 'd', GOP_DOUBLE, &dbl, NULL,
            "Print a double", "<double>"},
        {"float", 'f', GOP_FLOAT, &flt, NULL,
            "Print a float", "<float>"},
        {"integer", 'i', GOP_INT, &integer, NULL,
            "Print an integer", "<integer>"},
        {"long-integer", 'l', GOP_LONG_INT, &long_integer, NULL,
            "Print a long integer", "<long-integer>"},
        {"short", 'h', GOP_SHORT, &shrt, NULL,
            "Print a short", "<short>"},
        {"unsigned-short", 'u', GOP_UNSIGNED_SHORT, &ushrt, NULL,
            "Print an unsigned short", "<unsigned-short>"},
        GOP_TABLEEND
    };

    /* Get a new GOP context. */
    gop = gop_new();
    if (gop == NULL) {
        return EXIT_FAILURE;
    }

    /* Add the option tables. */
    gop_add_table(gop, NULL, options);

    /* This adds the automatic help output table. */
    gop_autohelp(gop);

    /* Parse the options. */
    gop_parse(gop, &argc, argv);

    /* Destroy the GOP context, as it is not needed anymore. */
    gop_destroy(gop);

    /* Print the variables that are not at the default value. */
    if (dbl != INFINITY) {
        printf("double = %f\n", dbl);
    }
    if (flt != INFINITY) {
        printf("float = %f\n", flt);
    }
    if (integer != -1) {
        printf("integer = %d\n", integer);
    }
    if (long_integer != -1) {
        printf("long-integer = %ld\n", long_integer);
    }
    if (shrt != -1) {
        printf("short = %d\n", shrt);
    }
    if (ushrt != USHRT_MAX) {
        printf("unsigned-short = %u\n", ushrt);
    }

    return EXIT_SUCCESS;
}
