/* @cotesdex license
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * @cotesdex */

/* @cotesdex title An exit status example */
/* @cotesdex description
 * Prerequisites:
 *  autohelp
 *  simple-parse
 *
 * This example shows how the exit status can be set using
 * gop_set_exit_status().
 * @cotesdex */

/* @cotesdex test
 *  @cotesdex status 0
 *  @cotesdex stderr
 *  @cotesdex stdout
 * @cotesdex */

/* @cotesdex test
 *  @cotesdex cmdline
 *  --help
 *  @cotesdex
 *  @cotesdex status 0
 *  @cotesdex stdout
 *   @cotesdex content
 *   Usage: exit-status [OPTION...]
 *   
 *         --error                        Trigger an error in GOP
 *         --exit                         Make GOP exit the program
 *         --error-status=ERROR_STATUS    Set the exit status on error
 *         --exit-status=EXIT_STATUS      Set the exit status on success
 *   
 *   Help options:
 *     -?, --help                         Show this help message
 *         --usage                        Display brief usage message
 *   @cotesdex */

/* @cotesdex test
 *  @cotesdex cmdline
 *  -?
 *  @cotesdex
 *  @cotesdex status 0
 *  @cotesdex stdout
 *   @cotesdex content
 *   Usage: exit-status [OPTION...]
 *   
 *         --error                        Trigger an error in GOP
 *         --exit                         Make GOP exit the program
 *         --error-status=ERROR_STATUS    Set the exit status on error
 *         --exit-status=EXIT_STATUS      Set the exit status on success
 *   
 *   Help options:
 *     -?, --help                         Show this help message
 *         --usage                        Display brief usage message
 *   @cotesdex */

/* @cotesdex test
 *  @cotesdex cmdline
 *  --usage
 *  @cotesdex
 *  @cotesdex status 0
 *  @cotesdex stdout
 *   @cotesdex content
 *   Usage: exit-status [-?] [--error] [--exit] [--error-status=ERROR_STATUS]
 *       [--exit-status=EXIT_STATUS] [-?|--help] [--usage]
 *   @cotesdex */

/* @cotesdex test
 *  @cotesdex cmdline
 *  --error
 *  @cotesdex
 *  @cotesdex status 1
 *  @cotesdex stdout
 * @cotesdex */

/* @cotesdex test
 *  @cotesdex cmdline
 *  --exit
 *  @cotesdex
 *  @cotesdex status 0
 *  @cotesdex stderr
 *  @cotesdex stdout
 * @cotesdex */

/* @cotesdex test
 *  @cotesdex cmdline
 *  --exit-status=10
 *  @cotesdex
 *  @cotesdex status 10
 *  @cotesdex stderr
 *  @cotesdex stdout
 * @cotesdex */

/* @cotesdex test
 *  @cotesdex cmdline
 *  --exit-status=10 --exit
 *  @cotesdex
 *  @cotesdex status 10
 *  @cotesdex stderr
 *  @cotesdex stdout
 * @cotesdex */

/* @cotesdex test
 *  @cotesdex cmdline
 *  --exit-status=20 --help
 *  @cotesdex
 *  @cotesdex status 20
 * @cotesdex */

/* @cotesdex test
 *  @cotesdex cmdline
 *  --exit-status=7 --usage
 *  @cotesdex
 *  @cotesdex status 7
 * @cotesdex */

/* @cotesdex test
 *  @cotesdex cmdline
 *  --exit-status=99 --error
 *  @cotesdex
 *  @cotesdex status 1
 *  @cotesdex stdout
 * @cotesdex */

/* @cotesdex test
 *  @cotesdex cmdline
 *  --error-status=79 --error
 *  @cotesdex
 *  @cotesdex status 79
 *  @cotesdex stdout
 * @cotesdex */

/* @cotesdex test
 *  @cotesdex cmdline
 *  --error-status=65 --error
 *  @cotesdex
 *  @cotesdex status 65
 *  @cotesdex stdout
 * @cotesdex */

/* @cotesdex test
 *  @cotesdex cmdline
 *  --error-status=79 --exit
 *  @cotesdex
 *  @cotesdex status 0
 *  @cotesdxex stderr
 *  @cotesdex stdout
 * @cotesdex */

#include <stdlib.h>

#include <gop.h>

/* This is set to 1 if the error status was set. */
static int error_status_set = 0;

/* Variables that GOP will fill in. */
static int error_status;
static int exit_status = EXIT_SUCCESS;

/* This is a function that is called when the --error option is given.
 * The function will trigger an error in GOP. */
static gop_return_t
error_callback(gop_t * const gop)
{
    /* An option callback is not allowed to return GOP_DO_ERROR so this
     * will trigger an error. */
    return GOP_DO_ERROR;
}

/* This function is called when --exit was given and will exit the
 * program by requesting exit to GOP. */
static gop_return_t
exit_callback(gop_t * const gop)
{
    /* Request exit. */
    return GOP_DO_EXIT;
}

/* This function is called when --error-status is given and error_status
 * is set. It only sets error_status_set to 1 so that the error handler
 * sets the error status. */
static gop_return_t
error_status_callback(gop_t * const gop)
{
    error_status_set = 1;
    return GOP_DO_CONTINUE;
}

/* This is a function that will be called when --exit-status is given
 * and exit_status is set.
 * It will set the exit status. */
static gop_return_t
exit_status_callback(gop_t * const gop)
{
    /* Set the exit status. */
    gop_set_exit_status(gop, exit_status);

    /* Go on processing options. */
    return GOP_DO_CONTINUE;
}

/* This function will be called when an error occured. It makes sure the
 * requested error exit status is set. This exit status should be
 * honoured by GOP. */
static gop_return_t
aterror_callback(gop_t * const gop)
{
    if (error_status_set) {
        gop_set_exit_status(gop, error_status);
    }

    /* Continue with default error handler. */
    return GOP_DO_ERROR;
}

int
main(int argc, char ** const argv)
{
    /* This is option table that is going to be used. */
    const gop_option_t options[] = {
        {"error", '\0', GOP_NONE, NULL, &error_callback,
            "Trigger an error in GOP", NULL},
        {"exit", '\0', GOP_NONE, NULL, &exit_callback,
            "Make GOP exit the program", NULL},
        {"error-status", '\0', GOP_INT, &error_status,
            &error_status_callback, "Set the exit status on error",
            "ERROR_STATUS"},
        {"exit-status", '\0', GOP_INT, &exit_status,
            &exit_status_callback, "Set the exit status on success",
            "EXIT_STATUS"},
        GOP_TABLEEND
    };

    gop_t * const gop = gop_new();
    if (gop == NULL) {
        return EXIT_FAILURE;
    }

    gop_aterror(gop, aterror_callback);
    gop_add_table(gop, NULL, options);
    gop_autohelp(gop);
    gop_parse(gop, &argc, argv);
    gop_destroy(gop);

    return exit_status;
}
