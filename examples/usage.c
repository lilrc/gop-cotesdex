/* @cotesdex license
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * @cotesdex */

/* @cotesdex title An exampe of the usage handling of GOP */
/* @cotesdex description
 * Prerequisites:
 *  autohelp
 * 
 * This example shows how the usage handling of GOP works with multiple usages.
 * @cotesdex */

/* @cotesdex test
 *  @cotesdex cmdline
 *  ah-muzencab
 *  @cotesdex
 *  @cotesdex status 0
 *  @cotesdex stderr
 *  @cotesdex stdout
 *   @cotesdex content
 *   God of the Bees
 *   @cotesdex */

/* @cotesdex test
 *  @cotesdex cmdline
 *  colel-cab
 *  @cotesdex
 *  @cotesdex status 0
 *  @cotesdex stderr
 *  @cotesdex stdout
 *   @cotesdex content
 *   Goddess of the Bees
 *   @cotesdex */

/* @cotesdex test
 *  @cotesdex cmdline
 *  --help
 *  @cotesdex
 *  @cotesdex status 0
 *  @cotesdex stderr
 *  @cotesdex stdout
 *   @cotesdex content
 *   Usage: usage ah-muzencab
 *     or:  usage colel-cab
 *     or:  usage [OPTIONS...]
 *   
 *   Help options:
 *     -?, --help     Show this help message
 *         --usage    Display brief usage message
 *   @cotesdex */

/* @cotesdex test
 *  @cotesdex cmdline
 *  -?
 *  @cotesdex
 *  @cotesdex status 0
 *  @cotesdex stderr
 *  @cotesdex stdout
 *   @cotesdex content
 *   Usage: usage ah-muzencab
 *     or:  usage colel-cab
 *     or:  usage [OPTIONS...]
 *   
 *   Help options:
 *     -?, --help     Show this help message
 *         --usage    Display brief usage message
 *   @cotesdex */

/* @cotesdex test
 *  @cotesdex cmdline
 *  --usage
 *  @cotesdex
 *  @cotesdex status 0
 *  @cotesdex stderr
 *  @cotesdex stdout
 *   @cotesdex content
 *   Usage: usage [-?] [-?|--help] [--usage]
 *   @cotesdex */

/* @cotesdex test
 *  @cotesdex status 1
 *  @cotesdex stderr
 *   @cotesdex content
 *   insufficient number of arguments
 *   @cotesdex
 *  @cotesdex stdout */

/* @cotesdex test
 *  @cotesdex cmdline
 *  ah-peku
 *  @cotesdex
 *  @cotesdex status 1
 *  @cotesdex stderr
 *   @cotesdex content
 *   invalid argument
 *   @cotesdex
 *  @cotesdex stdout */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <gop.h>

int
main(int argc, char ** const argv)
{
    gop_t * const gop = gop_new();
    if (gop == NULL) {
        return EXIT_FAILURE;
    }

    gop_add_usage(gop, "ah-muzencab");
    gop_add_usage(gop, "colel-cab");
    gop_add_usage(gop, "[OPTIONS...]");
    gop_autohelp(gop);
    gop_parse(gop, &argc, argv);
    gop_destroy(gop);

    if (argc != 2) {
        fputs("insufficient number of arguments\n", stderr);
        return EXIT_FAILURE;
    }

    if (strcmp(argv[1], "ah-muzencab") == 0) {
        puts("God of the Bees");
    } else if (strcmp(argv[1], "colel-cab") == 0) {
        puts("Goddess of the Bees");
    } else {
        fputs("invalid argument\n", stderr);
        return EXIT_FAILURE;
    }

    return EXIT_SUCCESS;
}
