/* @cotesdex license
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * @cotesdex */

/* @cotesdex title A test of argument descriptions of yesno options */
/* @cotesdex description
 * Prerequisites:
 *  simple-parse
 *  autohelp
 *  yesno
 * 
 * This is mostly a test to assert custom argument descriptions in
 * yesno options are favoured if such are given, but this works well
 * as an example of how it works even the other yesno example is more
 * useful.
 * 
 * This test and example does the following:
 * 
 * . Create a GOP context using +gop_new()+.
 * . Feed the GOP context with an option table using +gop_add_table()+. The
 *   table includes both yesno options.
 * . Add the automatic help output option table using +gop_autohelp()+.
 * . Parse the options using +gop_parse_options()+.
 * . Print the result.
 * . Destroy the context using +gop_destroy()+.
 * @cotesdex */

/* @cotesdex test
 *  @cotesdex status 0
 *  @cotesdex stderr
 *  @cotesdex stdout
 *   @cotesdex content
 *   Mary Shelley
 *   @cotesdex */

/* @cotesdex test
 *  @cotesdex cmdline
 *  --help
 *  @cotesdex
 *  @cotesdex status 0
 *  @cotesdex stdout
 *   @cotesdex content
 *   Usage: yesno-arg-desc [OPTION...]
 *   
 *         --austen=<no|yes>    Print 'Jane Austen' [no]
 *         --gothe=no|yes       Print 'Johann Wolfgang von Goethe' [no]
 *         --shelley=no,yes     Print 'Mary Shelley' [yes]
 *   
 *   Help options:
 *     -?, --help               Show this help message
 *         --usage              Display brief usage message
 *   @cotesdex */

/* @cotesdex test
 *  @cotesdex cmdline
 *  -?
 *  @cotesdex
 *  @cotesdex status 0
 *  @cotesdex stdout
 *   @cotesdex content
 *   Usage: yesno-arg-desc [OPTION...]
 *   
 *         --austen=<no|yes>    Print 'Jane Austen' [no]
 *         --gothe=no|yes       Print 'Johann Wolfgang von Goethe' [no]
 *         --shelley=no,yes     Print 'Mary Shelley' [yes]
 *   
 *   Help options:
 *     -?, --help               Show this help message
 *         --usage              Display brief usage message
 *   @cotesdex */

/* @cotesdex test
 *  @cotesdex cmdline
 *  --usage
 *  @cotesdex
 *  @cotesdex status 0
 *  @cotesdex stdout
 *   @cotesdex content
 *   Usage: yesno-arg-desc [-?] [--austen=<no|yes>] [--gothe=no|yes]
 *       [--shelley=no,yes] [-?|--help] [--usage]
 *   @cotesdex */

/* @cotesdex test
 *  @cotesdex cmdline
 *  --shelley=no
 *  @cotesdex
 *  @cotesdex status 0
 *  @cotesdex stderr
 *  @cotesdex stdout
 *   @cotesdex content
 *   @cotesdex */

/* @cotesdex test
 *  @cotesdex cmdline
 *  --shelley=no --gothe=yes
 *  @cotesdex
 *  @cotesdex status 0
 *  @cotesdex stderr
 *  @cotesdex stdout
 *   @cotesdex content
 *   Johann Wolfgang von Goethe
 *   @cotesdex */

/* @cotesdex test
 *  @cotesdex cmdline
 *  --shelley=no --gothe=yes --austen=yes
 *  @cotesdex
 *  @cotesdex status 0
 *  @cotesdex stderr
 *  @cotesdex stdout
 *   @cotesdex content
 *   Jane Austen
 *   Johann Wolfgang von Goethe
 *   @cotesdex */

/* @cotesdex test
 *  @cotesdex cmdline
 *  --gothe=yes --austen=yes --shelley=yes
 *  @cotesdex
 *  @cotesdex status 0
 *  @cotesdex stderr
 *  @cotesdex stdout
 *   @cotesdex content
 *   Jane Austen
 *   Johann Wolfgang von Goethe
 *   Mary Shelley
 *   @cotesdex */

/* @cotesdex test
 *  @cotesdex cmdline
 *  --austen=yes
 *  @cotesdex
 *  @cotesdex status 0
 *  @cotesdex stderr
 *  @cotesdex stdout
 *   @cotesdex content
 *   Jane Austen
 *   Mary Shelley
 *   @cotesdex */

/* @cotesdex test
 *  @cotesdex cmdline
 *  --gothe=yes
 *  @cotesdex
 *  @cotesdex status 0
 *  @cotesdex stderr
 *  @cotesdex stdout
 *   @cotesdex content
 *   Johann Wolfgang von Goethe
 *   Mary Shelley
 *   @cotesdex */

/* Standard headers. */
#include <stdio.h> /* printf() */
#include <stdlib.h> /* EXIT_SUCCESS, EXIT_FAILURE */

#include <gop.h>

int
main(int argc, char ** const argv)
{
    /* The variables GOP will fill in. */
    int austen = 0;
    int gothe = 0;
    int shelley = 1;

    const gop_option_t options[] = {
        {"austen", '\0', GOP_YESNO, &austen, NULL,
            "Print 'Jane Austen' [no]", "<no|yes>"},
        {"gothe", '\0', GOP_YESNO, &gothe, NULL,
            "Print 'Johann Wolfgang von Goethe' [no]", NULL},
        {"shelley", '\0', GOP_YESNO, &shelley, NULL,
            "Print 'Mary Shelley' [yes]", "no,yes"},
        GOP_TABLEEND
    };

    /* Get a new GOP context. */
    gop_t * const gop = gop_new();
    if (gop == NULL) {
        return EXIT_FAILURE;
    }

    gop_add_table(gop, NULL, options);
    gop_autohelp(gop);
    gop_parse(gop, &argc, argv);
    gop_destroy(gop);

    /* Print the requested animals. */
    if (austen) {
        printf("Jane Austen\n");
    }
    if (gothe) {
        printf("Johann Wolfgang von Goethe\n");
    }
    if (shelley) {
        printf("Mary Shelley\n");
    }

    return EXIT_SUCCESS;
}
