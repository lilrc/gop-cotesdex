/* @cotesdex license
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * @cotesdex */

/* @cotesdex title The terminal width */
/* @cotesdex description
 * Prerequisites:
 *  simple-parse
 *  autohelp
 * 
 * When GOP prints the help and usage output it uses the width of the terminal
 * to give the best possible result. By default GOP uses few ways of
 * automatically determining the terminal width, but it is also possible to
 * manually specify the desired terminal width using gop_set_termwidth().
 * 
 * In this example the desired width can be set using the --width option which
 * is hooked up gop_set_termwidth().
 * @cotesdex */

/* @cotesdex test
 *  @cotesdex cmdline
 *  --width=40 --help
 *  @cotesdex
 *  @cotesdex status 0
 *  @cotesdex stderr
 *  @cotesdex stdout
 *   @cotesdex content
 *   Usage: termwidth [OPTION...]
 *   This is a program description which will
 *   also be line broken properly. This
 *   string is made extra long to really show
 *   the line breaking features. Also line
 *   breaks should be handled properly. Look
 *   at this, for example.
 *   Yeah, that was a manual line break, but
 *   things really look better if two
 *   sections are separated by a double line
 *   break. Like this.
 *   
 *   See? Much nicer. End of description.
 *   
 *   Program options:
 *         --width=     Set the desired
 *               WIDTH   terminal width. This
 *                       option is
 *                       deliberately made
 *                       longer than
 *                       necessary to
 *                       illustrate how the
 *                       description will be
 *                       line broken in
 *                       accordance to the
 *                       terminal width
 *   
 *   Help options:
 *     -?, --help       Show this help
 *                       message
 *         --usage      Display brief usage
 *                       message
 *   @cotesdex */

/* @cotesdex test
 *  @cotesdex cmdline
 *  --width=45 --help
 *  @cotesdex
 *  @cotesdex status 0
 *  @cotesdex stderr
 *  @cotesdex stdout
 *   @cotesdex content
 *   Usage: termwidth [OPTION...]
 *   This is a program description which will also
 *   be line broken properly. This string is made
 *   extra long to really show the line breaking
 *   features. Also line breaks should be handled
 *   properly. Look at this, for example.
 *   Yeah, that was a manual line break, but
 *   things really look better if two sections are
 *   separated by a double line break. Like this.
 *   
 *   See? Much nicer. End of description.
 *   
 *   Program options:
 *         --width=      Set the desired terminal
 *                WIDTH   width. This option is
 *                        deliberately made longer
 *                        than necessary to
 *                        illustrate how the
 *                        description will be line
 *                        broken in accordance to
 *                        the terminal width
 *   
 *   Help options:
 *     -?, --help        Show this help message
 *         --usage       Display brief usage
 *                        message
 *   @cotesdex */

/* @cotesdex test
 *  @cotesdex cmdline
 *  --width=50 --help
 *  @cotesdex
 *  @cotesdex status 0
 *  @cotesdex stderr
 *  @cotesdex stdout
 *   @cotesdex content
 *   Usage: termwidth [OPTION...]
 *   This is a program description which will also be
 *   line broken properly. This string is made extra
 *   long to really show the line breaking features.
 *   Also line breaks should be handled properly. Look
 *   at this, for example.
 *   Yeah, that was a manual line break, but things
 *   really look better if two sections are separated
 *   by a double line break. Like this.
 *   
 *   See? Much nicer. End of description.
 *   
 *   Program options:
 *         --width=WIDTH  Set the desired terminal
 *                         width. This option is
 *                         deliberately made longer
 *                         than necessary to illustrate
 *                         how the description will be
 *                         line broken in accordance to
 *                         the terminal width
 *   
 *   Help options:
 *     -?, --help         Show this help message
 *         --usage        Display brief usage message
 *   @cotesdex */

/* @cotesdex test
 *  @cotesdex cmdline
 *  --width=55 --help
 *  @cotesdex
 *  @cotesdex status 0
 *  @cotesdex stderr
 *  @cotesdex stdout
 *   @cotesdex content
 *   Usage: termwidth [OPTION...]
 *   This is a program description which will also be line
 *   broken properly. This string is made extra long to
 *   really show the line breaking features. Also line
 *   breaks should be handled properly. Look at this, for
 *   example.
 *   Yeah, that was a manual line break, but things really
 *   look better if two sections are separated by a double
 *   line break. Like this.
 *   
 *   See? Much nicer. End of description.
 *   
 *   Program options:
 *         --width=WIDTH  Set the desired terminal width.
 *                         This option is deliberately made
 *                         longer than necessary to
 *                         illustrate how the description
 *                         will be line broken in accordance
 *                         to the terminal width
 *   
 *   Help options:
 *     -?, --help         Show this help message
 *         --usage        Display brief usage message
 *   @cotesdex */

/* @cotesdex test
 *  @cotesdex cmdline
 *  --width=60 --help
 *  @cotesdex
 *  @cotesdex status 0
 *  @cotesdex stderr
 *  @cotesdex stdout
 *   @cotesdex content
 *   Usage: termwidth [OPTION...]
 *   This is a program description which will also be line broken
 *   properly. This string is made extra long to really show the
 *   line breaking features. Also line breaks should be handled
 *   properly. Look at this, for example.
 *   Yeah, that was a manual line break, but things really look
 *   better if two sections are separated by a double line break.
 *   Like this.
 *   
 *   See? Much nicer. End of description.
 *   
 *   Program options:
 *         --width=WIDTH  Set the desired terminal width. This
 *                         option is deliberately made longer
 *                         than necessary to illustrate how the
 *                         description will be line broken in
 *                         accordance to the terminal width
 *   
 *   Help options:
 *     -?, --help         Show this help message
 *         --usage        Display brief usage message
 *   @cotesdex */

/* @cotesdex test
 *  @cotesdex cmdline
 *  --width=65 --help
 *  @cotesdex
 *  @cotesdex status 0
 *  @cotesdex stderr
 *  @cotesdex stdout
 *   @cotesdex content
 *   Usage: termwidth [OPTION...]
 *   This is a program description which will also be line broken
 *   properly. This string is made extra long to really show the line
 *   breaking features. Also line breaks should be handled properly.
 *   Look at this, for example.
 *   Yeah, that was a manual line break, but things really look better
 *   if two sections are separated by a double line break. Like this.
 *   
 *   See? Much nicer. End of description.
 *   
 *   Program options:
 *         --width=WIDTH  Set the desired terminal width. This option
 *                         is deliberately made longer than necessary
 *                         to illustrate how the description will be
 *                         line broken in accordance to the terminal
 *                         width
 *   
 *   Help options:
 *     -?, --help         Show this help message
 *         --usage        Display brief usage message
 *   @cotesdex */

/* @cotesdex test
 *  @cotesdex cmdline
 *  --width=70 --help
 *  @cotesdex
 *  @cotesdex status 0
 *  @cotesdex stderr
 *  @cotesdex stdout
 *   @cotesdex content
 *   Usage: termwidth [OPTION...]
 *   This is a program description which will also be line broken properly.
 *   This string is made extra long to really show the line breaking
 *   features. Also line breaks should be handled properly. Look at this,
 *   for example.
 *   Yeah, that was a manual line break, but things really look better if
 *   two sections are separated by a double line break. Like this.
 *   
 *   See? Much nicer. End of description.
 *   
 *   Program options:
 *         --width=WIDTH  Set the desired terminal width. This option is
 *                         deliberately made longer than necessary to
 *                         illustrate how the description will be line
 *                         broken in accordance to the terminal width
 *   
 *   Help options:
 *     -?, --help         Show this help message
 *         --usage        Display brief usage message
 *   @cotesdex */

/* @cotesdex test
 *  @cotesdex cmdline
 *  --width=75 --help
 *  @cotesdex
 *  @cotesdex status 0
 *  @cotesdex stderr
 *  @cotesdex stdout
 *   @cotesdex content
 *   Usage: termwidth [OPTION...]
 *   This is a program description which will also be line broken properly.
 *   This string is made extra long to really show the line breaking
 *   features. Also line breaks should be handled properly. Look at this, for
 *   example.
 *   Yeah, that was a manual line break, but things really look better if two
 *   sections are separated by a double line break. Like this.
 *   
 *   See? Much nicer. End of description.
 *   
 *   Program options:
 *         --width=WIDTH  Set the desired terminal width. This option is
 *                         deliberately made longer than necessary to illustrate
 *                         how the description will be line broken in accordance
 *                         to the terminal width
 *   
 *   Help options:
 *     -?, --help         Show this help message
 *         --usage        Display brief usage message
 *   @cotesdex */

/* @cotesdex test
 *  @cotesdex cmdline
 *  --width=80 --help
 *  @cotesdex
 *  @cotesdex status 0
 *  @cotesdex stderr
 *  @cotesdex stdout
 *   @cotesdex content
 *   Usage: termwidth [OPTION...]
 *   This is a program description which will also be line broken properly.
 *   This string is made extra long to really show the line breaking
 *   features. Also line breaks should be handled properly. Look at this, for
 *   example.
 *   Yeah, that was a manual line break, but things really look better if two
 *   sections are separated by a double line break. Like this.
 *   
 *   See? Much nicer. End of description.
 *   
 *   Program options:
 *         --width=WIDTH  Set the desired terminal width. This option is deliberately
 *                         made longer than necessary to illustrate how the
 *                         description will be line broken in accordance to the
 *                         terminal width
 *   
 *   Help options:
 *     -?, --help         Show this help message
 *         --usage        Display brief usage message
 *   @cotesdex */

/* @cotesdex test
 *  @cotesdex cmdline
 *  --width=85 --help
 *  @cotesdex
 *  @cotesdex status 0
 *  @cotesdex stderr
 *  @cotesdex stdout
 *   @cotesdex content
 *   Usage: termwidth [OPTION...]
 *   This is a program description which will also be line broken properly.
 *   This string is made extra long to really show the line breaking
 *   features. Also line breaks should be handled properly. Look at this, for
 *   example.
 *   Yeah, that was a manual line break, but things really look better if two
 *   sections are separated by a double line break. Like this.
 *   
 *   See? Much nicer. End of description.
 *   
 *   Program options:
 *         --width=WIDTH  Set the desired terminal width. This option is deliberately made
 *                         longer than necessary to illustrate how the description will be
 *                         line broken in accordance to the terminal width
 *   
 *   Help options:
 *     -?, --help         Show this help message
 *         --usage        Display brief usage message
 *   @cotesdex */

/* @cotesdex test
 *  @cotesdex cmdline
 *  --width=90 --help
 *  @cotesdex
 *  @cotesdex status 0
 *  @cotesdex stderr
 *  @cotesdex stdout
 *   @cotesdex content
 *   Usage: termwidth [OPTION...]
 *   This is a program description which will also be line broken properly.
 *   This string is made extra long to really show the line breaking
 *   features. Also line breaks should be handled properly. Look at this, for
 *   example.
 *   Yeah, that was a manual line break, but things really look better if two
 *   sections are separated by a double line break. Like this.
 *   
 *   See? Much nicer. End of description.
 *   
 *   Program options:
 *         --width=WIDTH  Set the desired terminal width. This option is deliberately made
 *                         longer than necessary to illustrate how the description will be line
 *                         broken in accordance to the terminal width
 *   
 *   Help options:
 *     -?, --help         Show this help message
 *         --usage        Display brief usage message
 *   @cotesdex */

/* @cotesdex test
 *  @cotesdex cmdline
 *  --width=40 --usage
 *  @cotesdex
 *  @cotesdex status 0
 *  @cotesdex stderr
 *  @cotesdex stdout
 *   @cotesdex content
 *   Usage: termwidth [-?] [--width=WIDTH]
 *       [-?|--help] [--usage]
 *   @cotesdex */

/* @cotesdex test
 *  @cotesdex cmdline
 *  --width=45 --usage
 *  @cotesdex
 *  @cotesdex status 0
 *  @cotesdex stderr
 *  @cotesdex stdout
 *   @cotesdex content
 *   Usage: termwidth [-?] [--width=WIDTH]
 *       [-?|--help] [--usage]
 *   @cotesdex */

/* @cotesdex test
 *  @cotesdex cmdline
 *  --width=50 --usage
 *  @cotesdex
 *  @cotesdex status 0
 *  @cotesdex stderr
 *  @cotesdex stdout
 *   @cotesdex content
 *   Usage: termwidth [-?] [--width=WIDTH] [-?|--help]
 *       [--usage]
 *   @cotesdex */

/* @cotesdex test
 *  @cotesdex cmdline
 *  --width=55 --usage
 *  @cotesdex
 *  @cotesdex status 0
 *  @cotesdex stderr
 *  @cotesdex stdout
 *   @cotesdex content
 *   Usage: termwidth [-?] [--width=WIDTH] [-?|--help]
 *       [--usage]
 *   @cotesdex */

/* @cotesdex test
 *  @cotesdex cmdline
 *  --width=60 --usage
 *  @cotesdex
 *  @cotesdex status 0
 *  @cotesdex stderr
 *  @cotesdex stdout
 *   @cotesdex content
 *   Usage: termwidth [-?] [--width=WIDTH] [-?|--help] [--usage]
 *   @cotesdex */

/* Standard headers. */
#include <stdlib.h> /* EXIT_SUCCESS, EXIT_FAILURE */

#include <gop.h>

static unsigned int width = 0;

static gop_return_t
cb_width(gop_t * gop)
{
    gop_set_termwidth(gop, width);
    return GOP_DO_CONTINUE;
}

int
main(int argc, char ** const argv)
{
    gop_t * gop;

    const gop_option_t options[] = {
        {"width", '\0', GOP_UNSIGNED_INT, &width, &cb_width,
            "Set the desired terminal width. This option is deliberately made "
            "longer than necessary to illustrate how the description will be "
            "line broken in accordance to the terminal width",
            "WIDTH"},
        GOP_TABLEEND
    };

    gop = gop_new();
    if (gop == NULL) {
        return EXIT_FAILURE;
    }

    gop_description(gop,
                    "This is a program description which will also be line "
                    "broken properly. This string is made extra long to really "
                    "show the line breaking features. Also line breaks should "
                    "be handled properly. Look at this, for example.\n"
                    "Yeah, that was a manual line break, but things really "
                    "look better if two sections are separated by a double "
                    "line break. Like this.\n"
                    "\n"
                    "See? Much nicer. End of description.");
    gop_add_table(gop, "Program options:", options);
    gop_autohelp(gop);
    gop_parse(gop, &argc, argv);
    gop_destroy(gop);

    return EXIT_SUCCESS;
}
