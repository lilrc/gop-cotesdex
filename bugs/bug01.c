/* @cotesdex license
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * @cotesdex */

/* @cotesdex title Bug 01 */
/* @cotesdex description
 * This bug caused GOP to completely ignore unknown short options when
 * options were parsed more than once and unkown options were ignored in
 * the first stage.
 *
 * The bug is reproducible with the following steps:
 * 1. Connect an error handler that discards GOP_ERROR_UNKSOPT.
 * 2. Parse options.
 * 3. Disconnect the previously connected error handler.
 * 4. Add an option table containing short options.
 * 5. Parse remaining options.
 *
 * The program does not print an error telling the user that the unknown
 * option was not found when unknown short options were given. The
 * program does not recognise short options in the second option table.
 *
 * The expected behaviour is that the program should complain when a
 * short option that is not in any of the tables is given and the
 * program should recognise short options that are in the second
 * option table.
 *
 * The bug was caused by GOP not adding unknown short options to the
 * unpaired arguments. Note that GOP stopped using an option pairing
 * technique for parsing options long ago.
 * @cotesdex */

/* @cotesdex test
 *  @cotesdex cmdline
 *  @cotesdex
 *  @cotesdex status 0
 *  @cotesdex stderr
 *  @cotesdex stdout
 *  @cotesdex
 * @cotesdex */

/* @cotesdex test
 *  @cotesdex cmdline
 *  -f
 *  @cotesdex
 *  @cotesdex status 0
 *  @cotesdex stderr
 *  @cotesdex stdout
 *   @cotesdex content
 *   flag given
 *   @cotesdex
 *  @cotesdex
 * @cotesdex */

/* @cotesdex test
 *  @cotesdex cmdline
 *  -a
 *  @cotesdex
 *  @cotesdex status 1
 *  @cotesdex stdout
 *  @cotesdex
 * @cotesdex */

#include <stdlib.h>
#include <stdio.h>

#include <gop.h>

static gop_return_t __attribute__((nonnull))
cb_aterror(gop_t * const gop)
{
    const gop_error_t error = gop_get_error(gop);
    switch (error) {
        case GOP_ERROR_UNKSOPT:
            /* ignore error */
            return GOP_DO_CONTINUE;
        default:
            /* issue default error handler */
            return GOP_DO_ERROR;
    }
}

static gop_return_t __attribute__((nonnull))
cb_flag(gop_t * const gop)
{
    puts("flag given");
    return GOP_DO_CONTINUE;
}

int
main(int argc, char ** const argv)
{
    const gop_option_t options[] = {
        {NULL, 'f', GOP_NONE, NULL, &cb_flag, NULL, NULL},
        GOP_TABLEEND
    };

    gop_t * const gop = gop_new();
    if (gop == NULL) {
        return EXIT_FAILURE;
    }

    /* 1. Connect the aterror function. */
    gop_aterror(gop, &cb_aterror);

    /* 2. Parse options. */
    gop_parse(gop, &argc, argv);

    /* 3. Disconnect the aterror function. */
    gop_aterror(gop, NULL);

    /* 4. Add the option table containing the short option. */
    gop_add_table(gop, NULL, options);

    /* 5. Parse remaining options. */
    gop_parse(gop, &argc, argv);

    gop_destroy(gop);
    return EXIT_SUCCESS;
}
