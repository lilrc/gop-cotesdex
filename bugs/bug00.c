/* @cotesdex license
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * @cotesdex */

/* @cotesdex title Bug 00 */
/* @cotesdex description
 * This bug caused GOP to wrongly interpret --base=argument as
 * --base+more=argument, where base and argument are arbitrary strings
 * and base+more is a string that starts with base.
 * 
 * The expected behaviour is to print an error if --base=argument is
 * given and the is no option corresponding to just base.
 * @cotesdex */

/* @cotesdex test
 *  @cotesdex cmdline
 *  @cotesdex
 *  @cotesdex status 0
 *  @cotesdex stderr
 *  @cotesdex stdout
 *   @cotesdex content
 *   something = NULL
 *   @cotesdex
 *  @cotesdex
 * @cotesdex */

/* @cotesdex test
 *  @cotesdex cmdline
 *  --something=Defoe
 *  @cotesdex
 *  @cotesdex status 0
 *  @cotesdex stderr
 *  @cotesdex stdout
 *   @cotesdex content
 *   something = Defoe
 *   @cotesdex
 *  @cotesdex
 * @cotesdex */

/* @cotesdex test
 *  @cotesdex cmdline
 *  --s=Voltaire
 *  @cotesdex
 *  @cotesdex status 1
 *  @cotesdex stdout
 *  @cotesdex
 * @cotesdex */

/* @cotesdex test
 *  @cotesdex cmdline
 *  --so=Voltaire
 *  @cotesdex
 *  @cotesdex status 1
 *  @cotesdex stdout
 *  @cotesdex
 * @cotesdex */

/* @cotesdex test
 *  @cotesdex cmdline
 *  --som=Voltaire
 *  @cotesdex
 *  @cotesdex status 1
 *  @cotesdex stdout
 *  @cotesdex
 * @cotesdex */

/* @cotesdex test
 *  @cotesdex cmdline
 *  --some=Voltaire
 *  @cotesdex
 *  @cotesdex status 1
 *  @cotesdex stdout
 *  @cotesdex
 * @cotesdex */

/* @cotesdex test
 *  @cotesdex cmdline
 *  --somet=Voltaire
 *  @cotesdex
 *  @cotesdex status 1
 *  @cotesdex stdout
 *  @cotesdex
 * @cotesdex */

/* @cotesdex test
 *  @cotesdex cmdline
 *  --someth=Voltaire
 *  @cotesdex
 *  @cotesdex status 1
 *  @cotesdex stdout
 *  @cotesdex
 * @cotesdex */

/* @cotesdex test
 *  @cotesdex cmdline
 *  --somethi=Voltaire
 *  @cotesdex
 *  @cotesdex status 1
 *  @cotesdex stdout
 *  @cotesdex
 * @cotesdex */

/* @cotesdex test
 *  @cotesdex cmdline
 *  --somethin=Voltaire
 *  @cotesdex
 *  @cotesdex status 1
 *  @cotesdex stdout
 *  @cotesdex
 * @cotesdex */

/* @cotesdex test
 *  @cotesdex cmdline
 *  --something=Voltaire
 *  @cotesdex
 *  @cotesdex status 0
 *  @cotesdex stderr
 *  @cotesdex stdout
 *   @cotesdex content
 *   something = Voltaire
 *   @cotesdex
 *  @cotesdex
 * @cotesdex */

#include <stdio.h>
#include <stdlib.h>

#include <gop.h>

int
main(int argc, char ** const argv)
{
    char * something = NULL;
    const gop_option_t options[] = {
        {"something", '\0', GOP_STRING, &something, NULL, NULL, NULL},
        GOP_TABLEEND
    };

    gop_t * const gop = gop_new();
    if (gop == NULL) {
        return EXIT_FAILURE;
    }

    gop_add_table(gop, NULL, options);
    gop_parse(gop, &argc, argv);
    gop_destroy(gop);

    fputs("something = ", stdout);
    if (something != NULL) {
        puts(something);
    } else {
        puts("NULL");
    }

    return EXIT_SUCCESS;
}
